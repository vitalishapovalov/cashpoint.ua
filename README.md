# Cashpoint project

Cashpoint.ua website redesign.

## Demo

The latest build of this project can be found [here](http://chis.kiev.ua/cashpoint/www/main.html)

## Installing

In order to start working with project, you must:

Clone repository to your local machine

```
git clone https://vitalishapovalov@bitbucket.org/vitalishapovalov/cashpoint.git
```

Install dependencies

```
npm i
```

## Gulp tasks

Watch for changes in templates/*.pug files

```
gulp watch-html
```

Watch for changes in styles/scss/*.scss files

```
gulp watch-css
```

Watch for changes in scripts/dev/*.js files

```
gulp watch-js
```

watch-html, watch-css and watch-js tasks

```
gulp dev
```

same as 'dev' but with uploading on remote server**

```
gulp dev-ftp
```

** In order to start work with ftp, you must edit ftp connection settings in gulpfile.js and then set username and password:

```
SET FTP_USER=(username for ftp-connection here)
SET FTP_PWD=(password for ftp-connection here)
```

## Built With

* [Gulp](http://gulpjs.com/)
* [Pug](https://github.com/pugjs/pug)
* [Sass](http://sass-lang.com/)
* [Pleeease.io](http://pleeease.io/)
* [Require.js](http://requirejs.org/)
* [Sourcemaps](https://www.npmjs.com/package/gulp-sourcemaps)

## URLs
* [Главная](http://chis.kiev.ua/cashpoint/www/main.html)
* [Как это работает](http://chis.kiev.ua/cashpoint/www/how_it_works.html)
* [Адреса](http://chis.kiev.ua/cashpoint/www/address.html)
* [Ближайшие отделения](http://chis.kiev.ua/cashpoint/www/address_nearest.html)
* [Оплата](http://chis.kiev.ua/cashpoint/www/payment.html)
* [Кредиты](http://chis.kiev.ua/cashpoint/www/credits.html)
* [О компании - Пресс-центр](http://chis.kiev.ua/cashpoint/www/about_press.html)
* [О компании - Вакансии](http://chis.kiev.ua/cashpoint/www/about_vacancies.html)
* [Новости](http://chis.kiev.ua/cashpoint/www/news.html)
* [FAQ](http://chis.kiev.ua/cashpoint/www/faq.html)
* [Личный кабинет - Проверка карты платоном](http://chis.kiev.ua/cashpoint/www/cabinet_cardCheck.html)
* [Личный кабинет - Текущий кредит](http://chis.kiev.ua/cashpoint/www/cabinet_currentCredits_1.html)
* [Личный кабинет - Текущий кредит (2)](http://chis.kiev.ua/cashpoint/www/cabinet_currentCredits_2.html)
* [Личный кабинет - Кредит успешно погашен](http://chis.kiev.ua/cashpoint/www/cabinet_currentCredits_3.html)
* [Личный кабинет - Нет заявок](http://chis.kiev.ua/cashpoint/www/cabinet_empty.html)
* [Личный кабинет - Заявка принята](http://chis.kiev.ua/cashpoint/www/cabinet_requestAccepted.html)
* [Личный кабинет - Настройки кабинета](http://chis.kiev.ua/cashpoint/www/cabinet_settings.html)
* [Личный кабинет - Мои заявки](http://chis.kiev.ua/cashpoint/www/cabinet_zayavki.html)
* [Личный кабинет - Персональные данные](http://chis.kiev.ua/cashpoint/www/cabinet_firstStep_confirm.html)
* [Личный кабинет - Ввод персональных данных](http://chis.kiev.ua/cashpoint/www/cabinet_firstStep.html)
* [Личный кабинет - Трудоустройство](http://chis.kiev.ua/cashpoint/www/cabinet_secondStep.html)
* [Личный кабинет - Социальная информация](http://chis.kiev.ua/cashpoint/www/cabinet_thirdStep.html)
* [Личный кабинет - Документы](http://chis.kiev.ua/cashpoint/www/cabinet_fourthStep_documents.html)
* [Личный кабинет - Проверка карты](http://chis.kiev.ua/cashpoint/www/cabinet_fourthStep_cardCheck.html)
* [Личный кабинет - Кредитный договор](http://chis.kiev.ua/cashpoint/www/cabinet_fourthStep_creditAgreement.html)

## Versioning

Current version is 2.0.2

## Authors

* **Shapovalov Vitali** - *Front-end* - [vitalishapovalov](https://bitbucket.org/vitalishapovalov/)