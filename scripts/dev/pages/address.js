define(['jquery', 'googleMap', 'depdrop'], 
	function($, googleMap, depdrop) {

		//screen width
	 	var screenWidth = $(window).width();

	 	//detect small-screen device
	 	var isMobile = screenWidth < 768 ? true : false;

		return {

	 		init: function() {

				var body = $('body'),
					main = $('main');

				var isAllAddressesPage = main.hasClass('address_all') ? true : false;
				var isNearestAddressPage = main.hasClass('address_nearest') ? true : false;

		 		//google map
		 		googleMap.init();

		 		/* nearest address page scripts */
		 		if (isNearestAddressPage) {

					body.on('click tap', '.address__office-search', function() {
						var _this = $(this);
						var container = _this.parent();

						var office_item = container.find('.offices-office');

						var defaultHeight = _this.height(), 
							itemsHeight   = 0;

						office_item.each(function(){
							itemsHeight = itemsHeight + $(this).outerHeight();
						});

						var fullHeight  = defaultHeight + itemsHeight;

						if (container.hasClass('opened'))
							container.removeClass('opened').css('height', defaultHeight);		
						else
							container.addClass('opened').css('height', fullHeight);
					});

					if (isMobile) {
						$('.address__header h1').text('ближайшие').css('visibility', 'visible');
					}
		 		}

		 		/* all addresses page scripts */
				if (isAllAddressesPage) {

					var regionSelect = $('select[name="region_id"]');
					var citySelect   = $('select[name="city_id"]');
					
			 		//dependent dropdown on selects
			 		$("#city_id").depdrop({
			 			"url":"http://cashpoint.ua/ru/department/city-list",
			 			"placeholder":"Город",
			 			"depends":["region_id"],
			 			"loadingText":"Загрузка..."
			 		});

					//select boxes (Адреса отделений)
					(function(){
						regionSelect.change(function(){
							if (regionSelect.val() !== 0) {
								citySelect.prop('disabled', false);
							}
							else {
								citySelect.prop('disabled', true);
							}
						});
					})();

					if (isMobile) {

						$('.address__office-search').click(function() {
							var _this = $(this);
							var container = _this.parent();

							if (container.hasClass('opened'))
								container.removeClass('opened')
							else
								container.addClass('opened')
						});

						body.on('click tap', '.address__tabs > span', function(){
							var _this     = $(this);
							var container = $('main');

							container.removeClass('address-address address-map');

							if (_this.hasClass('address__tabs-tab1')) {
								container.addClass('address-address');
							}
							else {
								container.addClass('address-map');
							}
						});

						regionSelect.change(function(){
							var _this  = $(this);
							var region = _this.find('option:selected').text();

							var output = $('#selected-region');

							if (region.length !== 0) {
								output.text(region + ' область');
							}
							else {
								output.text('Укажите область');
							}
						});

						citySelect.change(function(){
							var _this  = $(this);
							var city = _this.find('option:selected').text();

							var output = $('#selected-city');

							if (region.length !== 0) {
								output.text(', г.' + city);
							}
							else {
								output.text('');
							}
						});
					}
				}
	 		}
		}
});