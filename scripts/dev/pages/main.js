define(['jquery', 'googleMap', 'readyBxSlider', 'depdrop', 'calculator'],
	function($, googleMap, readyBxSlider, depdrop, calculator) {

		//screen width
	 	var screenWidth = $(window).width();

	 	//detect small-screen device
	 	var isMobile = screenWidth < 768;

		//count to the defined number when element is in viewport
		function countToNumber(element, number) {
			$({countNum: element.text()}).animate({countNum: number}, {
				duration: 3000,
			  	easing:'linear',
			  	step: function() {
			    	element.text(Math.floor(this.countNum));
			  	},
				complete: function() {
			    	element.text(number);
			  	}
			});	
		}

		//returns true if element (-65px) is in viewport
		function isScrolledIntoView(element) {
			var w = $(window);
			var docViewTop = $(window).scrollTop();
			var docViewBottom = docViewTop + w.height();
			var elemTop = element.offset().top;
			var elemBottom = elemTop + element.height();
			return ((docViewTop - 65 < elemTop) && (docViewBottom + 65 > elemBottom));
		}

		return {
	 		init: function() {

				// init calculator(with range sliders) scripts
				calculator.initCalculator($('.main_price_box'));

		 		//execute only on mobile devices
		 		if (isMobile) {

			 		//remove ripple effect from 'News' button (different styles from desktop ver.)
			 		$('#all_news').removeClass('ripple-button');

			 		////sliders on 2-nd and 4-th sections
			 		// show/hide arrows on slider functions (android 4.2 crutches)
					function showAllArrows( slider ) {
						slider
							.removeClass('lastSlide firstSlide')
							.addClass('middleSlide')
					}
					function showNextArrow( slider ) {
						slider
							.removeClass('middleSlide lastSlide')
							.addClass('firstSlide')
					}
					function showPrevArrow( slider ) {
						slider
							.removeClass('middleSlide firstSlide')
							.addClass('lastSlide')
					}
					//defining options
			 		var optionsForBx = {
			 			slideSelector: '.content_item',
			 			pager: false,
			 			swipeThreshold: 10,
			 			nextText: '',
			 			prevText: '',
			 			infiniteLoop: false,
			 			hideControlOnEnd: true,
						onSlideAfter: function( el ){
			 				var slider = el.parents('.bx-wrapper'),
								isLastSlide = el.next().length === 0,
								isFirstSlide = el.prev().length === 0;

			 				if ( isLastSlide ) {
								showPrevArrow( slider );
							} else if ( !isFirstSlide ) {
								showAllArrows( slider );
							}

							if ( isFirstSlide ) {
								showNextArrow( slider );
							} else if ( !isLastSlide ) {
								showAllArrows( slider );
							}
						},
						onSliderLoad: function() {
			 				var firstSliderControls = $('#second_screen').find('.bx-controls');
			 				var secondSliderControls = $('#fourth_screen').find('.bx-controls');

			 				if (firstSliderControls.length === 2) {
			 					firstSliderControls[0].remove();
			 				}
			 				if (secondSliderControls.length === 2) {
			 					secondSliderControls[0].remove();
			 				} 
			 			}
			 		};
			 		//apply to 2nd section
			 		var secondScreenSlider = $('#second_screen .content_box').bxSlider(optionsForBx);
			 		//apply to 4th section
			 		var fourthScreenSlider = $('#fourth_screen .content_box').bxSlider(optionsForBx);

			 		window.addEventListener("orientationchange", function() {
			 			secondScreenSlider.reloadSlider();
			 			fourthScreenSlider.reloadSlider();
					}, false);		
		 		}

		 		// unbind calculator modal
		 		$('.modal__openCalculator').click(function(e){
		 			e.stopPropagation();
		 			var body = $('html, body'); // HTML is also selected for mozilla (scrollTop animation)

		 			body.removeClass('side_menu_active');

		 			if (!isMobile)
		 				body.animate({scrollTop: $('#first_screen').offset().top}, 500);
		 			else if (isMobile)
		 				body.animate({scrollTop: ($('#first_screen').offset().top + 120)}, 500);
		 		});

		 		//google map
		 		googleMap.init();

		 		//dependent dropdown on selects
		 		$("#city_id").depdrop({
		 			"url":"http://cashpoint.ua/ru/department/city-list",
		 			"placeholder":"Город",
		 			"depends":["region_id"],
		 			"loadingText":"Загрузка..."
		 		});

				//numbers counting
				(function(){
					//"Выдано кредитов"
					var givenCreditsContainer = $('.credits_num');
					var givenCreditsCount = 67542;
					//"Городов"
					var citiesContainer = $('.cities_num');
					var citiesCount = 98;
					//"Агентов"
					var agentsContainer = $('.agents_num');
					var agentsCount = 304;

					function countNumbers() {
						if (isScrolledIntoView(givenCreditsContainer)) {
							countToNumber(givenCreditsContainer, givenCreditsCount);
						}
						if (isScrolledIntoView(citiesContainer)) {
							countToNumber(citiesContainer, citiesCount);
						}		
						if (isScrolledIntoView(agentsContainer)) {
							countToNumber(agentsContainer, agentsCount);
						}
					}

					// execute only on desktop and tablet
					if (!isMobile) {

						countNumbers();

						$(document).scroll(function(){
							countNumbers();
						});	
					}
				})();

				//select boxes (Адреса отделений)
				(function(){
					var regionSelect = $('select[name="region_id"]');
					var citySelect   = $('select[name="city_id"]');

					regionSelect.change(function(){
						if (regionSelect.val() !== 0) {
							citySelect.prop('disabled', false);
						}
						else {
							citySelect.prop('disabled', true);
						}
					});
				})();
	 		}
		}
});