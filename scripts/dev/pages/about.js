define(['jquery'], 
	
	function($) {

		return {

	 		init: function() {

				//vacancies page
				if ($('main').hasClass('vacancies')) {

					function openVacancy(vacancy) {
						vacancy.addClass('opened');
					}

					$('.cp_about__vacancy').click(function(e){

						if ($(e.target).closest($('.vacancy_arrow')).length > 0) {
							return;
						}
						else {
							var th = $(this);
							var opened = th.hasClass('opened');

							if (!opened) {
								openVacancy(th);
							}
							else {
								return;
							}	
						}
					});

					$('.vacancy_arrow').click(function(){

						var thisContainer = $(this).parent();

						if (thisContainer.hasClass('opened')) {
							thisContainer.removeClass('opened');
						}
						else {
							openVacancy(thisContainer);
						}
					})
				}
	 		}
		}
});