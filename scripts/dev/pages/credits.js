define(['jquery', 'requestCalculator', 'maskedInput'], 
	function($, requestCalculator, maskedInput) {

		return {
	 		init: function() {
				//phone number mask
				$('input[type="tel"]').mask("+38 (099) 999-99-99");

				//calculator validation on click
				var options = {
					container: 		$('main > .credits_calculator'),
					sendButton: 	'#calculator__send',
					depDropDepend:  '#guestcredit-department_id',
					depDropDepends: 'guestcredit-city_id',
					nameInput: 		'.calculator__input-container.name input',
					summInputId: 	'calculator__summ-input',
					timeInputId: 	'calculator__time-input'
				};
				requestCalculator.initRequestCalculator(options);
	 		}
		}
});