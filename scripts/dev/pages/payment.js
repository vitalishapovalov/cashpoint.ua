define(['jquery'], function($){
    return {
        init: function() {
            $('input[type="tel"]').mask("+38 (099) 999-99-99");
            $('#payment__login').click(function(){
                loginValidation($(this));
            });
        }
    }
});