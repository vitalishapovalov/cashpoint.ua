define(['jquery'], 
	function($) {

	var body = $('body'),
		main = $('main');

	// animates progressbar to specified number
	function setSecondaryProgressbarTo(number) {
		var progressbar = $('.progressbar_fill');
		var percentage  = $('.progressbar_percent span');

		progressbar.css('width',''+number+'%');
		$({countNum: percentage.text()}).animate({countNum: number}, {
			duration: 1000,
			easing:'linear',
			step: function() {
				percentage.text(Math.floor(this.countNum));
			},
			complete: function() {
				percentage.text(number);
			}
		});
	}

	// animates main progressbar to specified percent
	function setMainProgressbarTo(percent) {
		var mainProgressBar = $('.steps__steps_indicator .indicator');

		mainProgressBar.animate({'width':''+percent+'%'}, 1000);
	}

	// move both progressbars
	function setBothProgressbars(mainNum, secondaryNum) {
		setMainProgressbarTo( mainNum );
		setSecondaryProgressbarTo( secondaryNum );
	}

	return {

		// current credits page scripts
		currentCreditsPageInit: function() {

			// get current credits - table
			if (main.hasClass('table')) {
				// validate and send data (current credits table page)
				body.on('click tap', '#current_credits_payment', function(){
					var value 		  = $('.current_credits__payment-input input').val();
					var warning_label = $('.current_credits__payment-input .warning');

					warning_label.removeClass('active');

					if (value.length < 2 || value == 0) {
						warning_label.addClass('active');
					}
					else {
						$.ajax({
							url: './ajax/return_success.json',
							type: 'GET',
							data: value,
							success: function(res) {
								alert('Value: '+ value + '\nresponse: ' + res.response)
							}
						});
					}
				});
			}

			// get total payment summ - form 
			else if (main.hasClass('form')) {
				/**/
			}

			// current credits with successful payment message
			else if (main.hasClass('message')) {
				/**/
			}
		},

		// credit card input/check page
		creditCardCheckInit: function() {
			// animate progress bar to 90%
			(function(){
				setSecondaryProgressbarTo(90);
			})();

			require(['creditCardValidation'], function(creditCardValidation){
				
				// bug fix
				$('input.ccjs-csc').on('input', function(){
					$(this).removeClass('ccjs-error');
				});
				$('.ccjs-expiration').removeClass('ccjs-error');
				$('.selectStyler').removeClass('ccjs-error');
				$('select').on('change', function(){
					var container = $('.selectStyler.month');
					if ( $('select.ccjs-month :selected').val() === '0' ) {
						container.addClass('ccjs-error');
						container.parent().addClass('ccjs-error');
					} else {
						container.removeClass('ccjs-error');
						container.parent().removeClass('ccjs-error');
					}
				});

				// validate and send credit card data
				$('#validate_credit_card').on('click tap', function(){

					var card_number_input = $('.ccjs-number .ccjs-number-formatted');
					var expire_month 	  = $('.ccjs-month');
					var expire_year 	  = $('.ccjs-year');
					var cvv 			  = $('.ccjs-csc input[name="csc"]');

					if (creditcardjs.isValid()) {
						// generate data with credit card data
						var data = JSON.stringify({
							cardNumber: card_number_input.val(),
							expire: {
								month: expire_month.val(),
								year: expire_year.val()
							},
							cvv: cvv.val()
						});
						// send data
						$.ajax({
							url: './ajax/return_success.json',
							type: 'POST',
							data: data,
							success: function(res) {
								alert('Data: '+ data + '\nresponse: ' + res.response)
							}
						});
					}

					else if (!creditcardjs.isValid()) {
						var validate_objects = {
							card_number_input: card_number_input,
							expire: expire_month.parent().parent(),
							cvv: cvv
						};
						for (var obj in validate_objects) {
							var object = validate_objects[obj];
							if (!object.hasClass('ccjs-complete')) {
								object.addClass('ccjs-error')
							}
						}
					}
				});
			});
		},

		// Accepted request page
		requestAcceptedInit: function() {
			/* */
		},

		// 'STEPS' pages
		getCreditStepsInit: function() {
			require(['getCreditSteps'], function(getCreditSteps){
                // % of the main progressbar (top of the page) and secondary (bottom)
                var mainNum, secondaryNum;
				// common scripts for all steps
				getCreditSteps.init.common();

				// first step
				if ( main.hasClass( 'step_1' ) ) {
					getCreditSteps.init.firstStep();

					if ( main.hasClass( 'confirm' ) ) {
						mainNum = 32;
						secondaryNum = 25;
					} else {
						mainNum = 5.5;
						secondaryNum = 3;
					}
				// second step
				} else if ( main.hasClass( 'step_2' ) ) {
					getCreditSteps.init.secondStep();
                    mainNum = 32;
                    secondaryNum = 25;
				// third step
				} else if ( main.hasClass( 'step_3' ) ) {
					getCreditSteps.init.thirdStep();
                    mainNum = 65;
                    secondaryNum = 50;
				} else if ( main.hasClass( 'step_4' ) ) {
					getCreditSteps.init.fourthStep();
                    if ( main.hasClass('documents') ) {
                    	mainNum = 82;
                        secondaryNum = 75;
                    } else if ( main.hasClass('card_check') ) {
						mainNum = 97;
                        secondaryNum = 100;
                        setTimeout( function(){ main.addClass('pb_finished') }, 500 )
                    } else if ( main.hasClass('credit_agreement') ) {
						mainNum = 97;
                        secondaryNum = 100;
                    }
				}
                setBothProgressbars(mainNum, secondaryNum);
			})
		},

		// cabinet settings
		cabinetSettingsInit: function(){
			// DOM elements
			var els = {
				pass: {
					container: $('.settings__content--column.change_pass'),
					old: $('.settings__content .oldPass'),
					new: $('.settings__content .newPass'),
					repNew: $('.settings__content .repNewPass'),
					save:  $('#save_password')
				},
				email: {
					container: $('.settings__content--column.change_email'),
					old: $('.settings__content .oldEmail'),
					new: $('.settings__content .newEmail'),
					newPass: $('.settings__content .newEmailPass'),
					save: $('#save_email')
				}
			};

			// collect inputs
			els.pass.inputContainers = els.pass.container.find('.modal__input-container');
			els.email.inputContainers = els.email.container.find('.modal__input-container');

			// warn with warn message
			function warn( el, message ) {
				if ( typeof message === 'string' )
					el.find('.modal__label-warning').text( message );
				el.addClass( 'incorrect' )
			}

			// change view on success
            function changeView() {
                $('main').addClass('success');
            }

			// change email
			els.email.save.on( 'click tap', function(){
				// helper indicator
				var email_is_validated = false;

				var element = els.email.old,
					input = element.find('input'),
					oldValue = input.val();

				els.email.inputContainers.removeClass('incorrect');

				if ( oldValue.length != 0 ) {

					if( isValidEmail( oldValue ) ) {

						var data = {
							event: 'confirmCurrentEmail',
							email: oldValue
						};

						$.ajax({
							url: './ajax/return_success.json',
							type: 'GET',
							data: JSON.stringify( data ),
							success: function( res ) {
								console.log('Data: ' + JSON.stringify( data ) + '\nResponse: ' + JSON.stringify( res ));
								if ( res.response === 'success' ) {
									email_is_validated = true;
								} else {
									warn( element, 'Введите корректный старый email' );
								}
							},
							error: function() {
								alert('При передаче данных произошла ошибка! Проверьте своё подключение к интернету и повторите попытку.');
							}
						}).then( function(){
							if ( email_is_validated ) {
								var element = els.email.new,
									input = element.find('input'),
									newValue = input.val();

                                if ( oldValue != newValue ) {
                                    if ( isValidEmail( newValue ) ) {
                                        var element = els.email.newPass,
                                            input = element.find('input'),
                                            passwordValue = input.val();

                                        if ( passwordValue.length > 0 ) {

                                            var data = {
                                                event: 'confirmCurrentPassword',
                                                password: passwordValue
                                            };

                                            $.ajax({
                                                url: './ajax/return_success.json',
                                                type: 'GET',
                                                data: JSON.stringify( data ),
                                                success: function( res ) {
                                                    console.log('Data: ' + JSON.stringify( data ) + '\nResponse: ' + JSON.stringify( res ));
                                                    if ( res.response === 'success' ) {
                                                        var data = {
                                                            event: 'setUpNewEmail',
                                                            newEmail: newValue
                                                        };
                                                        $.ajax({
                                                            url: './ajax/return_success.json',
                                                            type: 'GET',
                                                            data: JSON.stringify( data ),
                                                            success: function( res ) {
                                                                console.log('Data: ' + JSON.stringify( data ) + '\nResponse: ' + JSON.stringify( res ));
                                                                changeView();
                                                            },
                                                            error: function() {
                                                                alert('При передаче данных произошла ошибка! Проверьте своё подключение к интернету и повторите попытку.');
                                                            }
                                                        })
                                                    } else {
                                                        warn( element, 'Введите корректный пароль' );
                                                    }
                                                },
                                                error: function() {
                                                    alert('При передаче данных произошла ошибка! Проверьте своё подключение к интернету и повторите попытку.');
                                                }
                                            })

                                        } else {
                                            warn( element, 'Введите пароль' );
                                        }
                                    } else {
                                        warn( element, 'Email некорректного формата' );
                                    }
                                } else {
                                    warn( element, 'Новый email не должен совпадать со старым' )
                                }
							}
						});
					} else {
						warn( element, 'Email некорректного формата' );
					}
				} else {
					warn( element, 'Введите email' );
				}
			});

            // change password
            els.pass.save.on('click tap', function(){
                // helper indicator
                var password_is_validated = false;

                var element = els.pass.old,
                    input = element.find('input'),
                    value = input.val();

                els.pass.inputContainers.removeClass('incorrect');

                if ( value.length > 0 ) {

                    var data = {
                        event: 'confirmCurrentPassword',
                        password: value
                    };

                    $.ajax({
                        url: './ajax/return_success.json',
                        type: 'GET',
                        data: JSON.stringify( data ),
                        success: function( res ) {
                            console.log('Data: ' + JSON.stringify( data ) + '\nResponse: ' + JSON.stringify( res ));
                            if ( res.response === 'success' ) {
                                password_is_validated = true;
                            } else {
                                warn( element, 'Введите корректный пароль' );
                            }
                        },
                        error: function() {
                            alert('При передаче данных произошла ошибка! Проверьте своё подключение к интернету и повторите попытку.');
                        }
                    }).then(function(){
                        var newPassElement = els.pass.new,
                            repNewPassElem = els.pass.repNew,
                            newPassInput = newPassElement.find('input'),
                            repNewPassInput = repNewPassElem.find('input'),
                            newPassValue = newPassInput.val(),
                            repNewPassValue = repNewPassInput.val();

                        if ( newPassValue.length >= 6 ) {
                            if ( value != newPassValue ) {
                                if ( newPassValue === repNewPassValue ) {

                                    var data = {
                                        event: 'setUpNewPassword',
                                        newPassword: newPassValue
                                    };

                                    $.ajax({
                                        url: './ajax/return_success.json',
                                        type: 'GET',
                                        data: JSON.stringify( data ),
                                        success: function( res ) {
                                            console.log('Data: ' + JSON.stringify( data ) + '\nResponse: ' + JSON.stringify( res ));
                                            changeView();
                                        },
                                        error: function() {
                                            alert('При передаче данных произошла ошибка! Проверьте своё подключение к интернету и повторите попытку.');
                                        }
                                    })
                                } else {
                                    warn( repNewPassElem, 'Пароли не совпадают' )
                                }
                            } else {
                                warn( newPassElement, 'Новый пароль не должен совпадать со старым' )
                            }
                        } else {
                            warn( newPassElement, 'Минимальная длинна пароля - 6 символов' )
                        }
                    });
                } else {
                    warn( element, 'Введите пароль' )
                }
            });
		}
	}
});
	