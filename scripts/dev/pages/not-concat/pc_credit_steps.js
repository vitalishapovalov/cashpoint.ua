define(['calculator', 'datepicker', 'maskedInput'],
    function(calculator, datepicker, maskedInput){

    var body = $('body'),
        main = $('main');

    var isMobile = function() {
        return $(window).width() < 768
    };

    // set localstorage data
    function setLocalStorage() {
        var sum = main.find('#number_summ').val(),
            time = main.find('#number_date').val();
        localStorage.setItem( 'credit', sum );
        localStorage.setItem( 'time', time );
        localStorage.setItem( 'pay', $('span#credit_final_using_online').text() );
    }

    // wrong input warning
    function warn(label, message) {
        if (typeof message === 'string') {
            label.find('.err').text(message);
        }
        label.addClass('wrong');
    }

    // confirm correct input
    function confirm(label) {
        label.removeClass('wrong');
    }

    //validate user's INN
    function validateInn(label) {
        var input = label.find('input'),
            value = input.val();

        if ( !value.length ) {
            warn( label, 'Введите Ваш ИНН' );
            return false;
        } else if ( value.length < 10 || value.length > 10 ) {
            warn( label, 'ИНН состоит из 10 цифр' );
            return false;
        } else if ( value.length === 10 ) {
            confirm( label );
            return true;
        }
    }

    //validate passport series
    function validatePassportSeries(label) {
        var input = label.find('input'),
            value = input.val();

        if ( !value.length ) {
            warn( label, "Заполните" );
            return false;
        } else if ( value.length != 2 ) {
            warn( label, "Ошибка" );
            return false;
        } else {
            confirm( label );
            return true;
        }
    }

    // validate passport number
    function validatePassportNumber(label) {
        var input = label.find('input'),
            value = input.val();

        if ( !value.length ) {
            warn( label, 'Введите номер паспорта' );
            return false;
        } else if ( value.length < 6 || value.length > 6 ) {
            warn( label, 'Номер состоит из 6 цифр' );
            return false;
        } else if ( value.length === 6 ) {
            confirm( label );
            return true;
        }
    }

    // validate input field (name, surname etc)
    function validateInputText(label) {
        var input = label.find('input'),
            value = input.val();

        if ( value.length < 1 ) {
            warn( label );
            return false;
        } else {
            confirm( label );
            return true;
        }
    }

    // validate textarea field
    function validateTextareaText(label) {
        var input = label.find('textarea'),
            value = input.val();

        if ( value.length < 2 ) {
            warn( label );
            return false;
        } else {
            confirm( label );
            return true;
        }
    }

    // validate select field
    function validateSelectText(label) {
        var input = label.find('select'),
            value = input.find('option:selected').val();

        if ( +value === 0 ) {
            warn( label );
            return false;
        } else {
            confirm( label );
            return true;
        }
    }

    // validate tel field
    function validateTel(label) {
        var input = label.find('input'),
            valueLength = input.val().length;

        if ( valueLength != 19 ) {
            warn( label );
            return false;
        } else {
            confirm( label );
            return true;
        }
    }

    // 'Process private data' accepted
    function validatePrivateDataAcceptance(label) {
        var input        = label.find('input[name="personalDataProcessing"]'),
            userAccepted = input.serialize().split('=')[1] === 'true';
        if (userAccepted) {
            confirm( label );
            return true;
        } else {
            warn( label );
            return false;
        }
    }

    // generate credit data (summ, time)
    function generateCreditInfo(summEl, timeEl) {
        var data = ['credit_summ=' + summEl.text(), 'credit_time=' + timeEl.text()];
        data = data.join('&');
        return data;
    }

    //generate data from all visible inputs + credit data
    function generateData(summEl, timeEl) {
        var data = $("#main_steps_form :input:visible").serialize();
        data = data + '&' + generateCreditInfo(summEl, timeEl);
        return data;
    }

    // validate all visible inputs (text and tel) and selects
    function validateInputsAndSelects() {
        $('#main_steps_form :input:visible').each( function(){
            var $el    = $( this ),
                name   = $el.attr('name'),
                type   = $el.attr('type'),
                parent = $el.parents( '.step_fieldset-field' );

            if ( $el.is('select') ) {
                validateSelectText( parent )
            } else if ( $el.is('input') ) {
                if ( type === 'tel' )
                    validateTel( parent );
                else
                    validateInputText( parent )
            }
        });
    }

    // check for 'wrong' labels in form; returns TRUE if form is filled correctly
    function formIsValid() {
        return $( '#main_steps_form label:visible.wrong' ).length === 0
    }

    return {

        init: {

            // common scripts for all steps pages
            common: function() {

                //common DOM elements
                var calculatorContainer    = $('.steps__calculator_info'),
                    calculatorBox          = $('main.getCredit_steps .price_box'),
                    calculatorToggleButton = $('.calculator_info_toggle'),
                    progressbarContainer   = $('.progressbar_container'),
                    credit_summ            = $('.credit_summ'),
                    credit_time            = $('.credit_time'),
                    credit_using_online    = $('span#credit_final_using_online');

                // get user-chosen credit sum and time
                var credit = {
                    sum: localStorage.getItem('credit') || 1000,
                    time: localStorage.getItem('time') || 10
                };

                // set user-chosen data
                credit_summ.text(credit.sum);
                credit_time.text(credit.time);

                // show content
                [credit_summ, credit_time].forEach( function( el ){
                    el
                        .parent()
                        .parent()
                        .css('opacity','1');
                });

                // calculator
                var options = {
                    output: {
                        using: {
                            card: credit_using_online
                        },
                        sum: credit_summ,
                        time: credit_time
                    }
                };
                calculator.initCalculator(calculatorBox, options);
                calculator.set.values(credit.sum, credit.time);

                // move progressbar and show main content
                if ( isMobile() ) {
                    progressbarContainer.insertAfter( calculatorContainer );
                    main.addClass( 'visible mutated' );
                }

                // catch screen orientation/size change and control progressbar position
                $( window ).on( "orientationchange resize", function() {
                    var mutated = main.hasClass('mutated');
                    if ( isMobile() ) {
                        if ( !mutated ) {
                            progressbarContainer.insertAfter( calculatorContainer );
                            main.addClass( 'visible mutated' )
                        }
                    } else if ( mutated ) {
                        main.append( progressbarContainer );
                        main.removeClass( 'mutated' )
                    }
                });

                // dropdown button
                calculatorToggleButton.on('click tap', function() {
                    $(this).parent().toggleClass('opened');
                });
            },

            // step 1 scripts
            firstStep: function() {
                // DOM elements
                var elements = {
                        userContactPhone         : $('.userContactPhone'),
                        userContactEmail         : $('.userContactEmail'),
                        userInn                  : $('.userInn'),
                        userPassportSeries       : $('.userPassportSeries'),
                        userPassportNumber       : $('.userPassportNumber'),
                        userPassportGivenDate    : $('.userPassportGivenDate'),
                        userPassportGivenBy      : $('.userPassportGivenBy'),
                        userSex                  : $('.userSex'),
                        userPassportName         : $('.userPassportName'),
                        userPassportSurname      : $('.userPassportSurname'),
                        userPassportSecondName   : $('.userPassportSecondName'),
                        userPassportBirthdayDate : $('.userPassportBirthdayDate'),
                        userAddressRegion        : $('.userAddressRegion'),
                        userAddressCity          : $('.userAddressCity'),
                        userAddressStreet        : $('.userAddressStreet'),
                        userAddressHouse         : $('.userAddressHouse'),
                        userAddressApartment     : $('.userAddressApartment'),
                        personalDataProcessing   : $('.personalDataProcessing'),
                        // field for users that do not live in registration address
                        userRegistrationAddressRegion        : $('.userRegistrationAddressRegion'),
                        userRegistrationAddressCity          : $('.userRegistrationAddressCity'),
                        userRegistrationAddressStreet        : $('.userRegistrationAddressStreet'),
                        userRegistrationAddressHouse         : $('.userRegistrationAddressHouse'),
                        userRegistrationAddressApartment     : $('.userRegistrationAddressApartment')
                    };

                // credit data containers
                var credit_summ = $('.credit_summ'),
                    credit_time = $('.credit_time');

                // buttons
                var get_user_data    = $('#get_user_data'),
                    confirm_continue = $('#confirm-continue');

                // radio buttons
                var address_radio = $('.step_radioset.address input[type="radio"]');

                // containers
                var userRegistrationAddress = $('.userRegistrationAddress');

                // (Это, своего рода, удар в бубен. Ровняем чекбоксы после невиданного бага)
                $('.step_radioset-container .radio-control').css('z-index','1');

                // calculate age function
                function getAge(dateString) {
                    var date = dateString.split('.');
                    var today = new Date();
                    var birthDate = new Date(date[2], date[1], date[0]);
                    var age = today.getFullYear() - birthDate.getFullYear();
                    var m = today.getMonth() - birthDate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                        age--;
                    }
                    return age;
                }
                // set age under the 'Birthday date' field
                function setAge(date) {
                    elements.userPassportBirthdayDate
                        .parent()
                        .find( '.years_old' )
                        .text( getAge( date ) )
                }

                // datepicker plugin
                [ elements.userPassportGivenDate, elements.userPassportBirthdayDate ].forEach( function( el ){
                    el
                        .find( 'input' )
                        .prop( 'readonly', true)
                        .datepicker({
                            maxDate: new Date(),
                            // count Age
                            onSelect: function( fdate, date , inst ) {
                                if ( inst.$el.attr( 'name' ) === 'userPassportBirthdayDate' )
                                    setAge(fdate)
                            }
                        });
                });

                // the page with saved and confirmed user information
                var isConfirmPage = main.hasClass('confirm');

                // this is default 'Personal info' page
                if (!isConfirmPage) {

                    // get user data clicked
                    get_user_data.on('click tap', function(){

                        //validate data
                        var inn = validateInn(elements.userInn),
                            ser = validatePassportSeries(elements.userPassportSeries),
                            num = validatePassportNumber(elements.userPassportNumber);

                        if (inn && ser && num) {
                            // generate data
                            var data = {
                                event: "userDataAutofill",
                                inn: elements.userInn.find('input').val(),
                                passportSeries: elements.userPassportSeries.find('input').val().toUpperCase(),
                                passportNumber: elements.userPassportNumber.find('input').val()
                            };
                            // get data
                            $.ajax({
                                url: './ajax/getCredit_step1.json',
                                type: 'GET',
                                dataType: 'json',
                                cache: false,
                                data: data,
                                success: function(res) {
                                    console.log("event: userDataAutofill\n" + JSON.stringify(res));
                                    for (var field in res) {
                                        var input;
                                        if (res.hasOwnProperty(field)) {
                                            if (field === 'userPassportGivenBy') {
                                                input = elements[field].find('textarea');
                                            } else if (field === 'userAddressRegion'
                                                || field === 'userRegistrationAddressRegion'
                                                || field === 'userSex' ) {
                                                input = elements[field].find('select');
                                                input.addClass('active');
                                            } else if (field === 'userAddressMatch' ){
                                                $( '.step_radioset.address' )
                                                    .find( 'input[name=userAddressMatch]' )
                                                    .prop('checked', true)
                                                    .parent()
                                                    .addClass('active');
                                                $('.userRegistrationAddress').addClass('active')
                                            } else {
                                                input = elements[field].find('input:not([type="checkbox"]):not([type="radio"])');
                                            }
                                            input.val(res[field]);
                                        }
                                    }
                                    // count age
                                    setAge( res.userPassportBirthdayDate );
                                },
                                error: function() {
                                    alert('При передаче данных произошла ошибка! Проверьте своё подключение к интернету и повторите попытку.');
                                }
                            })
                        }
                    });

                    // show/hide additional info
                    address_radio.on('change', function(){
                        userRegistrationAddress.toggleClass('active');
                    });

                    // only alphabetic characters input
                    $('.alphachars').each( function(){
                        onlyAlphaCharsInput( $( this ) );
                    });

                    // main submit button
                    confirm_continue.on('click tap', function(){
                        var infoValidated = false;

                        // validate data ( sorry )
                        var inn = validateInn(elements.userInn),
                            ser = validatePassportSeries(elements.userPassportSeries),
                            num = validatePassportNumber(elements.userPassportNumber),
                            sex = validateSelectText(elements.userSex),
                            nam = validateInputText(elements.userPassportName),
                            sur = validateInputText(elements.userPassportSurname),
                            sec = validateInputText(elements.userPassportSecondName),
                            cit = validateInputText(elements.userAddressCity),
                            str = validateInputText(elements.userAddressStreet),
                            hou = validateInputText(elements.userAddressHouse),
                            apa = validateInputText(elements.userAddressApartment),
                            giv = validateTextareaText(elements.userPassportGivenBy),
                            dat = validateInputText(elements.userPassportBirthdayDate),
                            bir = validateInputText(elements.userPassportGivenDate),
                            reg = validateSelectText(elements.userAddressRegion),
                            acc = validatePrivateDataAcceptance(elements.personalDataProcessing);

                        var defaultFieldsValidated = sex && dat && bir && acc && inn && ser && num && nam && sur && sec && cit && str && hou && apa && giv && reg;

                        var registrationAddressActive = userRegistrationAddress.hasClass('active');

                        if (registrationAddressActive) {
                            var reg_cit = validateInputText(elements.userRegistrationAddressCity),
                                reg_str = validateInputText(elements.userRegistrationAddressStreet),
                                reg_hou = validateInputText(elements.userRegistrationAddressHouse),
                                reg_apa = validateInputText(elements.userRegistrationAddressApartment),
                                reg_reg = validateSelectText(elements.userRegistrationAddressRegion);

                            if (defaultFieldsValidated && reg_cit && reg_str && reg_hou && reg_apa && reg_reg) {
                                infoValidated = true;
                            }

                        } else if (defaultFieldsValidated) {
                                infoValidated = true;
                        }

                        if (infoValidated) {
                            // generate data
                            var data = generateData(credit_summ, credit_time);
                            // local data
                            setLocalStorage();
                            // send data
                            $.ajax({
                                url: './ajax/return_success.json',
                                type: 'POST',
                                data: data,
                                success: function(res) {
                                    alert('Data: ' + data + '\nResponse: ' + JSON.stringify(res));
                                    window.location.href = './cabinet_secondStep.html';
                                },
                                error: function() {
                                    alert('При передаче данных произошла ошибка! Проверьте своё подключение к интернету и повторите попытку.');
                                }
                            });
                        }
                    });
                } else if (isConfirmPage) {
                    // find input in element and set it's value
                    function setValue( el, type, value ){
                        el.find(type).val(value);
                    }
                    // find input with specified attribute,
                    // check it and add 'active' class to parent
                    function checkInput( el, attr, attrVal ) {
                        el
                            .find( 'input[' + attr + '=' + attrVal + ']' )
                            .prop('checked', true)
                            .parent()
                            .addClass('active');
                    }

                    // TODO: Find out - should i remove this?
                    // get information about user
                    $.ajax({
                        url: './ajax/getCredit_step1_confirm.json',
                        type: 'GET',
                        cache: false,
                        data: JSON.stringify({event:"getUserData"}),
                        success: function(res) {
                            console.log("event: getUserData \nData: " + JSON.stringify(res));
                            for ( var field in res ) {
                                if ( res.hasOwnProperty(field) ) {

                                    if ( field === 'userPassportGivenBy' ) {
                                        setValue( elements[field], 'textarea', res[field] );

                                    } else if ( field === 'userAddressRegion'
                                        || field === 'userRegistrationAddressRegion'
                                        || field === 'userSex' ) {
                                        setValue( elements[field], 'select', res[field] );

                                    } else if ( field === 'userAddressMatch' ) {
                                        var container = $( '.step_radioset.address' );
                                        checkInput( container, 'value', res[field] );
                                        if ( res[field] == false )
                                            userRegistrationAddress.addClass( 'active' );

                                    } else if ( field === 'notifySms' || field === 'notifyEmail' ) {
                                        if (res[field] === 'on')
                                            checkInput( $('.step_radioset.notify'), 'name', field );

                                    } else {
                                        setValue( elements[field], 'input:not([type="checkbox"]):not([type="radio"])', res[field] );
                                    }
                                }
                            }
                            // count age
                            setAge( res.userPassportBirthdayDate );
                        },
                        error: function() {
                            alert('При передаче данных произошла ошибка! Проверьте своё подключение к интернету и повторите попытку.');
                        }
                    });

                    // show modal message, when user click on input
                    body.on('click tap', function(e){
                        var target = $(e.target);
                        if ( target.closest('input:disabled, textarea:disabled, select:disabled').length > 0 || target.closest('.disabled-overflow').length > 0 )
                            openModal('modal_cannot_modify_data');
                    });

                    //close modal event on 'OK' button click
                    $('.modal__cannot_modify_data .modal__button').on('click tap', function(){
                        closeModals();
                    });

                    // re-send credit data
                    confirm_continue.on('click tap', function(){
                        // generate data
                        var data = generateCreditInfo(credit_summ, credit_time);
                        // local data
                        setLocalStorage();
                        // send data
                        $.ajax({
                            url: './ajax/return_success.json',
                            type: 'POST',
                            data: data,
                            success: function(res) {
                                alert('Data: ' + data + '\nResponse: ' + JSON.stringify(res));
                                window.location.href = './cabinet_secondStep.html';
                            },
                            error: function() {
                                alert('При передаче данных произошла ошибка! Проверьте своё подключение к интернету и повторите попытку.');
                            }
                        })
                    });
                }
            },

            // step 2 scripts
            secondStep: function() {
                // DOM elements
                var elements = {
                    userEmploymentType: $('.userEmploymentType'),
                    userActivityField:  $('.userActivityField'),
                    userCompanyName:    $('.userCompanyName'),
                    userJobPosition:    $('.userJobPosition'),
                    userEmploymentDate: $('.userEmploymentDate'),
                    userMonthlyIncome:  $('.userMonthlyIncome'),
                    userWorkTel:        $('.userWorkTel'),
                    userContactPerson:  $('.userContactPerson'),
                    userHomeTel:        $('.userHomeTel')
                };
                // credit data containers
                var credit_summ = $('.credit_summ'),
                    credit_time = $('.credit_time');
                // collect all disabled items
                var disabled = $(':disabled:not(option)');
                //buttons
                var confirm_continue = $('#confirm-continue');

                //phone mask
                $('input[type="tel"]').mask("+38 (099) 999-99-99");

                // only alphabetic characters input
                $( '.alphachars' ).each( function(){
                    onlyAlphaCharsInput( $( this ) );
                });

                //datepicker
                elements.userEmploymentDate
                    .find( 'input' )
                    .datepicker({
                        maxDate: new Date(),
                    });

                // shows all elements and then hides @arrayToHide elements.
                // if @arrayToHide isn't passed - only show all elements
                function showAllAndHide( arrayToHide ) {
                    for ( var el in elements ) {
                        elements[el].show();
                    }
                    if ( arrayToHide instanceof Array )
                        arrayToHide.forEach( function( el ){
                            el.hide();
                        })
                }

                // switch label's text in Date element on different selects
                function switchDateLabel( type ) {
                    var el      = elements.userEmploymentDate,
                        elInput = el.find('input'),
                        elNode  = el.get(0),
                        elText  = elNode.childNodes[0],
                        elValue = elText.nodeValue.trim();

                    // set date label
                    function setDateLabel( nodeValue, nameProp ){
                        elText.nodeValue = nodeValue;
                        elInput.prop('name', nameProp)
                    }

                    if ( elValue === 'Дата приема на работу' && type == 2 )
                        setDateLabel( 'Дата оформления СПД', 'userSpdDate' );
                    else if ( elValue === 'Дата оформления СПД' && type != 2 )
                        setDateLabel( 'Дата приема на работу', 'userEmploymentDate' )
                }

                // switch label's text in 'Phone' element on different selects
                function switchPhoneLabel( type ) {
                    var el      = elements.userWorkTel,
                        elNode  = el.get(0),
                        elText  = elNode.childNodes[0];

                    // set phone label
                    function setPhoneLabel( nodeValue){
                        elText.nodeValue = nodeValue
                    }

                    if ( type == 2 )
                        setPhoneLabel( 'Телефон по месту работы или лицо, подтверждающее трудоустройство');
                    else if ( type != 2 )
                        setPhoneLabel( 'Телефон по месту работы (руководителя / бухгалтера / кадров)')
                }

                elements.userEmploymentType
                    .find( 'select' )
                    .on( 'change', function(){
                        var type = $( this ).val(),
                            arrayToHide = [];

                        if ( type == 1 ) {
                            for ( var el in elements ) {
                                if ( el != 'userEmploymentType' && el != 'userHomeTel' && el != 'userMonthlyIncome' )
                                    arrayToHide.push( elements[el] )
                            }
                            showAllAndHide( arrayToHide )
                        } else if ( type == 2 ) {
                            arrayToHide = [elements.userHomeTel, elements.userJobPosition];
                            showAllAndHide( arrayToHide );
                        } else if ( type == 3 ) {
                            showAllAndHide();
                        }

                        switchDateLabel( type );
                        switchPhoneLabel( type )
                    }).one( 'change', function(){
                        disabled.prop( 'disabled', false )
                    });

                // 'NEXT' button
                confirm_continue.on( 'click tap', function(){
                    var employmentTypeSelected = validateSelectText( elements.userEmploymentType );

                    if (employmentTypeSelected) {
                        // validation
                        validateInputsAndSelects();

                        if (formIsValid()) {
                            // generate data
                            var data = generateData(credit_summ, credit_time);
                            // local data
                            setLocalStorage();
                            // send data
                            $.ajax({
                                url: './ajax/return_success.json',
                                type: 'POST',
                                data: data,
                                success: function(res) {
                                    alert('Data: ' + data + '\nResponse: ' + JSON.stringify(res));
                                    window.location.href = './cabinet_thirdStep.html';
                                },
                                error: function() {
                                    alert('При передаче данных произошла ошибка! Проверьте своё подключение к интернету и повторите попытку.');
                                }
                            })
                        }
                    }
                })
            },

            // third step scripts
            thirdStep: function(){
                // DOM elements
                var elements = {
                    userFamilyStatus:   $('.userFamilyStatus'),
                    userRealEstate:     $('.userRealEstate'),
                    userChildrenAmount: $('.userChildrenAmount'),
                    userFindOut:        $('.userFindOut')
                };
                // credit data containers
                var credit_summ = $('.credit_summ'),
                    credit_time = $('.credit_time');
                //buttons
                var confirm_continue = $('#confirm-continue');

                //phone mask
                $('input[type="tel"]').mask("+38 (099) 999-99-99");

                // only alphabetic characters input
                $( '.alphachars' ).each( function(){
                    onlyAlphaCharsInput( $( this ) );
                });

                // validate and send
                confirm_continue.on( 'click tap', function(){
                    // validation
                    validateInputsAndSelects();

                    if (formIsValid()) {
                        // generate data
                        var data = generateData(credit_summ, credit_time);
                        // local data
                        setLocalStorage();
                        // send data
                        $.ajax({
                            url: './ajax/return_success.json',
                            type: 'POST',
                            data: data,
                            success: function(res) {
                                alert('Data: ' + data + '\nResponse: ' + JSON.stringify(res));
                                window.location.href = './cabinet_fourthStep_documents.html';
                            },
                            error: function() {
                                alert('При передаче данных произошла ошибка! Проверьте своё подключение к интернету и повторите попытку.');
                            }
                        })
                    }
                })
            },

            // fourth step scripts
            fourthStep: function(){
                // DOCUMENTS page
                if ( main.hasClass('documents') ) {
                    // DOM elements
                    var elements = {
                        examples: {
                            userFacePhotoExample    : $('#userFacePhotoExample'),
                            userPassportPhotoExample: $('#userPassportPhotoExample')
                        },
                        existing: {
                            facePhoto    : $('#userExistingFacePhoto'),
                            passportPhoto: $('#userExistingPassportPhoto')
                        },
                        newMade: {
                            facePhoto    : $('.new_content.facePhoto'),
                            passportPhoto: $('.new_content.passportPhoto ')
                        },
                        correctPhotoExample: $('.correctPhotoExample'),
                        newPhotoUpload     : $('.newPhotoUpload'),
                        mainForm           : $('#main_steps_form')
                    };
                    //buttons
                    var document_example_toggle = $('.document_example-toggle'),
                        new_photoUpload_toggle  = $('.new_photoUpload-toggle'),
                        confirm_continue        = $('#confirm-continue');

                    // toggle examples block
                    document_example_toggle.on('click tap', function(){
                        elements.correctPhotoExample.toggleClass('active');
                    });
                    // toggle new photos block
                    new_photoUpload_toggle.on('click tap', function(){
                        elements.newPhotoUpload.toggleClass('active');
                    });

                    // file upload controller
                    var Upload_controller = function( el ) {
                        this._container    = el;
                        this._input        = this._container.find('input');
                        this._closeButton  = this._container.find('.existing_close');
                        this._image        = this._container.find('.preview_img');

                        this.statuses = {
                            error: 'error',
                            uploading: 'file_uploading',
                            uploaded: 'file_uploaded',
                            dragOver: 'drag_over'
                        };
                    };
                    // methods
                    Upload_controller.prototype = {

                        setStatus: function( status ) {
                            this._container.addClass( status );
                            return this;
                        },

                        removeStatus: function ( status ) {
                            this._container.removeClass( status );
                            return this;
                        },

                        setDefaultStatus: function() {
                            for ( var status in this.statuses )
                                if ( this.statuses.hasOwnProperty( status ) )
                                    this._container.removeClass( this.statuses[status] );
                            return this;
                        },

                        setUploadingStatus: function() {
                            this.setStatus( this.statuses.uploading );
                            return this;
                        },

                        setUploadedStatus: function() {
                            this.setStatus( this.statuses.uploaded );
                            return this;
                        },

                        setErrorStatus: function() {
                            this
                                .removeStatus( this.statuses.uploading )
                                .setStatus( this.statuses.error );
                            return this;
                        },

                        setDragoverStatus: function() {
                            this.setStatus( this.statuses.dragOver );
                            return this;
                        },

                        setImage: function( src ) {
                            this._image.attr( 'src', src );
                            return this;
                        },

                        removeImage: function() {
                            this.setImage( '' );
                            return this;
                        },

                        removeFile: function() {
                            this._input.val('');
                            return this;
                        },

                        defaultState: function() {
                            this
                                .setDefaultStatus()
                                .removeImage()
                                .removeFile();
                            return this;
                        },

                        uploadedState: function() {
                            this
                                .setDefaultStatus()
                                .setUploadedStatus();
                            return this;
                        },

                        errorState: function() {
                            var _self = this;
                            _self.setErrorStatus();
                            if ( _self.checkImageExistence() )
                                setTimeout( function(){
                                    _self.removeStatus( _self.statuses.error )
                                }, 5000 );
                            return this;
                        },

                        bindCloseButton: function() {
                            var _self = this;
                            _self._closeButton.on( 'click tap', function(){
                                _self.defaultState()
                            });
                            return this;
                        },

                        checkImageExistence: function() {
                            return this._image.attr('src').length > 1
                        },

                        generateFileToSend: function() {
                            var blob     = this._input[0].files[0],
                                formData = new FormData();
                            formData.append('fileToUpload', blob);

                            return formData
                        },

                        // bind file input el to clear it's value on every click
                        bindFileUploadCleaning: function() {
                            $(this._input).on('click tap', function(){
                                this.value = null;
                            });
                            return this;
                        },

                        fileData: function( reader ) {
                            var dataURL = reader.result;
                            var dataType = dataURL.substr(0, 10);

                            return {
                                url: dataURL,
                                type: dataType
                            }
                        },

                        animateOnDrag: function() {
                            var _self   = this,
                                counter = 0;

                            function applyDragover( e ) {
                                e.preventDefault();
                                counter++;
                                _self._container.addClass( _self.statuses.dragOver )
                            }

                            function removeDragover() {
                                counter--;
                                if (counter === 0) {
                                    _self._container.removeClass( _self.statuses.dragOver )
                                }
                            }

                            _self._container.bind({
                                dragenter: function( e ) {
                                    applyDragover( e )
                                },

                                dragleave: function() {
                                    removeDragover()
                                },

                                drop: function() {
                                    removeDragover()
                                }
                            });

                            return this;
                        },

                        // listen for changes
                        listen: function () {

                            var _self = this;

                            $( _self._input ).change( function() {

                                // something was loaded
                                if ( _self._input[0].files && _self._input[0].files[0] ) {

                                    // loading...
                                    _self.setUploadingStatus();

                                    // create new file-ready instance
                                    var reader = new FileReader();

                                    // read image as dataURL (for preview)
                                    reader.readAsDataURL( _self._input[0].files[0] );

                                    // preview image
                                    reader.onload = function() {
                                        var _this = this;
                                        // any kind of image was uploaded
                                        if ( _self.fileData(_this).type === 'data:image' ) {
                                            $.ajax({
                                                url: './ajax/return_success.json',
                                                type: 'GET',
                                                processData: false,
                                                contentType: false,
                                                data: _self.generateFileToSend(),
                                                success: function( res ) {
                                                    if ( res.response === 'success' ) {
                                                        console.log("File sent. Response: " + JSON.stringify(res));
                                                        _self
                                                            .setImage( _self.fileData(_this).url )
                                                            .uploadedState();
                                                    } else {
                                                        _self.errorState();
                                                    }
                                                },
                                                error: function() {
                                                    alert('При передаче данных произошла ошибка! Проверьте своё подключение к интернету и повторите попытку.');
                                                }
                                            })
                                        } else {
                                            _self.errorState();
                                        }
                                    };
                                }
                            });

                            return this;
                        },

                        init: function() {
                            this
                                .bindCloseButton()
                                .listen()
                                .bindFileUploadCleaning()
                                .animateOnDrag();
                            return this;
                        }
                    };

                    // handle images upload
                    for ( var el in elements.existing ) {
                        var obj = new Upload_controller( elements.existing[el] );
                        obj.init();
                    }

                    // webcam and mobile cam
                    require(['webcam'], function( Webcam ) {

                        // indicator to handle only single webcam instance at once
                        var cameraIsActive = false;

                        // controller
                        var Camera_controller = function( button ) {
                            this._container = button.parents('.new_content');
                            this._button = this._container.find('.new_button');
                            this._close = this._container.find('.new_close');
                            this._image = this._container.find('img');
                            this._input = this._container.find('input');
                            this._preview = this._container.find('.new_previewContainer');
                            this._previewContainer = this._container.find('.new_uploadedPhotoContainer');

                            this.states = {
                                webcam: 'activeWebCam',
                                image: 'activeImage'
                            }
                        };
                        Camera_controller.prototype = {

                            isDevice: function(){
                                return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
                            },

                            resetInstance: function(){
                                var resetInstance = (new Camera_controller ( this._button )).launch();
                            },

                            closeAnotherInstance: function(){
                                // close webcam
                                this.closeWebcam();
                                // fint another instance
                                var instance = main.find('.activeWebCam');
                                // remove status
                                instance.removeClass('activeWebCam');
                                // unbind events and create empty instance
                                var newInstance = new Camera_controller( instance.find('.new_button') );
                                newInstance.launch();
                            },

                            setState: function( state ){
                                this._container.addClass( state );
                                return this;
                            },

                            unsetState: function( state ) {
                                this._container.removeClass( state );
                                return this;
                            },

                            setWebcamState: function() {
                                this.setState( this.states.webcam );
                                return this;
                            },

                            setImageState: function() {
                                this
                                    .setDefaultState()
                                    .setState( this.states.image );
                                return this;
                            },

                            setDefaultState: function() {
                                for ( var state in this.states )
                                    this.unsetState( this.states[state] );
                                return this;
                            },

                            // bind file input el to clear it's value on every click
                            bindFileUploadCleaning: function() {
                                $(this._input).on('click tap', function(){
                                    this.value = null;
                                });
                                return this;
                            },

                            listenForFiles: function(){
                                var _self = this;
                                $( _self._input ).change( function() {

                                    // something was loaded
                                    if ( _self._input[0].files && _self._input[0].files[0] ) {

                                        // create new file-ready instance
                                        var reader = new FileReader();

                                        // read image as dataURL (for preview)
                                        reader.readAsDataURL( _self._input[0].files[0] );

                                        // preview image
                                        reader.onload = function( e ) {
                                            var newUrl = e.target.result;
                                            _self
                                            // set image-ready state
                                                .setImageState()
                                                // attach new image
                                                ._image
                                                .attr('src', newUrl)
                                                .show();
                                            // bind 'close' button
                                            _self.closeClickHandle().removeImage();

                                        };
                                    }
                                });
                                return this;
                            },

                            closeWebcam: function(){
                                Webcam.reset();
                                cameraIsActive = false;
                                return this;
                            },

                            setCloseClickHandler: function( func ) {
                                var _self = this;
                                // bind 'Close button'
                                this._close
                                    .unbind('click tap')
                                    .on('click tap', function(){
                                        if ( typeof func === 'function' ) {
                                            func.call(_self);
                                            _self.setDefaultState();
                                        }
                                    });
                                return this;
                            },

                            closeClickHandle: function(){
                                function shutDownWebcam() {
                                    this
                                        .closeWebcam()
                                        .handleDesktopClick()
                                }
                                function removeImage() {
                                    this._image
                                        .attr('src','')
                                        .hide()
                                }
                                function removeImageAndInstance() {
                                    removeImage.call(this);
                                    this.resetInstance();
                                }
                                var _self = this;
                                return {
                                    shutDownWebcam: function(){
                                        _self
                                            .setCloseClickHandler( shutDownWebcam )
                                    },
                                    removeImage: function(){
                                        _self.setCloseClickHandler( removeImage )
                                    },
                                    removeImageAndInstance: function(){
                                        _self.setCloseClickHandler( removeImageAndInstance )
                                    }
                                }
                            },

                            launchWebcam: function(){
                                Webcam.attach( this._preview[0] );
                                cameraIsActive = true;
                                return this;
                            },

                            launchMobile: function(){
                                // invisible input covers the button completely now
                                this
                                    .listenForFiles()
                                    .bindFileUploadCleaning()
                                    ._input.show();
                                return this;
                            },

                            handleSnapClick: function() {
                                var _self = this;
                                this._button.unbind('click').on('click tap', function(){
                                    Webcam.snap( function( data_uri ){
                                        _self._image
                                            .attr('src', data_uri)
                                            .show()
                                    });
                                    // change interface
                                    _self.closeWebcam().setImageState();
                                    // unbind main button
                                    _self._button.unbind('click');
                                    // re-bind 'close' button
                                    _self.closeClickHandle().removeImageAndInstance();
                                });
                                return this;
                            },

                            handleDesktopClick: function(){
                                var _self = this;
                                // handle button click
                                _self._button.unbind('click tap').on('click tap', function(){
                                    // if camera is already launched at another instance
                                    if ( cameraIsActive ) _self.closeAnotherInstance();
                                    // bind 'Close' button
                                    _self.closeClickHandle().shutDownWebcam();
                                    // bind main button
                                    _self
                                        .launchWebcam()
                                        .setWebcamState()
                                        .handleSnapClick()
                                });
                                return this;
                            },

                            launch: function(){
                                if ( this.isDevice() )
                                    this.launchMobile();
                                else
                                    this.handleDesktopClick();
                                return this;
                            }
                        };

                        // webcam settings
                        Webcam.set({
                            width: 300,
                            height: 220,
                            //output file config
                            dest_width: 640,
                            dest_height: 480,
                            image_format: 'jpeg',
                            jpeg_quality: 90,
                            force_flash: false
                        });
                        //handle error
                        Webcam.on('error', function( err ){
                            alert(err.message);
                            $('.activeWebCam').removeClass('activeWebCam');
                            $('.new_button').unbind('click')
                        });

                        // handle webcam
                        for ( var el in elements.newMade ) {
                            var element = elements.newMade[el].find('.new_button');
                            var obj = new Camera_controller( element );
                            obj.launch();
                        }
                    });

                    // validate and send
                    confirm_continue.on('click tap', function(){

                        function photoExists( container ){
                            return container.find('img').attr('src').length > 1
                        }

                        function checkAndPush( containers, objToPushWhere ){
                            containers.forEach( function( el ){
                                if ( photoExists( el ) ) {
                                    var fieldName = el.attr('data-image-type'),
                                        image = el.find('img');

                                    objToPushWhere[fieldName] = image.attr('src');
                                }
                            });
                        }

                        var facePhotoUploaded     = photoExists( elements.existing.facePhoto ) || photoExists( elements.newMade.facePhoto ),
                            passportPhotoUploaded = photoExists( elements.existing.passportPhoto ) || photoExists( elements.newMade.passportPhoto );

                        // set default state
                        elements.mainForm.removeClass('error');

                        if ( facePhotoUploaded && passportPhotoUploaded ) {
                            // local data
                            setLocalStorage();
                            // generate data to send
                            var data = {};
                            var imageContainers = [ elements.existing.facePhoto, elements.newMade.facePhoto,
                                elements.existing.passportPhoto, elements.newMade.passportPhoto ];
                            checkAndPush( imageContainers, data );

                            //send data
                            $.ajax({
                                url: './ajax/return_success.json',
                                type: 'POST',
                                data: data,
                                success: function( res ) {
                                    console.log(data);
                                    alert('Data: files\nResponse: ' + JSON.stringify(res));
                                    window.location.href = './cabinet_fourthStep_cardCheck.html';
                                },
                                error: function() {
                                    alert('При передаче данных произошла ошибка! Проверьте своё подключение к интернету и повторите попытку.');
                                }
                            })
                        } else {
                            elements.mainForm.addClass('error')
                        }
                    });
                }
                // CREDIT CARD CHECK PAGE
                else if ( main.hasClass('card_check') ) {
                    // DOM elements
                    var elements = {
                        bank_phone_numbers: $('.bank-phone-numbers'),
                        checkSum          : $('.step_fieldset-field.checkSumm')
                    };
                    // buttons
                    var confirm_continue = $('#confirm-continue');

                    // validate and send 
                    confirm_continue.on('click tap', function() {
                        var container    = elements.checkSum,
                            input        = container.find('input'),
                            value        = input.val(),
                            inputIsValid = value.length > 0;

                        if (inputIsValid) {
                            //send data
                            $.ajax({
                                url: './ajax/return_success.json',
                                type: 'GET',
                                data: JSON.stringify({
                                    event: 'checkCreditCardSumm',
                                    data: value
                                }),
                                success: function( res ) {
                                    if ( res.response === 'success' ) {
                                        alert('event: checkCreditCardSumm\nResponse: ' + JSON.stringify(res));
                                        window.location.href = './cabinet_fourthStep_creditAgreement.html';
                                    } else {
                                        warn( container, 'Указанная сумма некорректна' )
                                    }
                                },
                                error: function() {
                                    alert('При передаче данных произошла ошибка! Проверьте своё подключение к интернету и повторите попытку.');
                                }
                            })
                        } else {
                            warn( container, 'Введите корректную сумму' )
                        }
                    });
                }
            }
        }
    }
});