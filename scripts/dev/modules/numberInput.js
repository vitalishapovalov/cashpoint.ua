define(['numberInputPolyfill'], function(numberInputPolyfill) {

	return {

		init: function() {
			var inputs = document.querySelector('input[type="number"]');
			for (var i = 0, len = inputs.length; i < len; i++) {
				inputTypeNumberPolyfill.polyfillElement(inputs[i]);
			}
		}
	}
});