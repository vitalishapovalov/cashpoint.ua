define(['jquery', 'numberInput'], function($, numberInput){

	return {

		pushValues: function(options) {
			var container = options.container || $('body');
			var summ 	  = options.summ || '';
			var time 	  = options.time || '';

			container
				.find('.calculator__input-container.summ')
				.find('input')
				.val(summ);
			container
				.find('.calculator__input-container.time')
				.find('input')
				.val(time)
		},

		initRequestCalculator: function(options) {

			var container     = options.container || $('body');
			var sendButton    = container.find(options.sendButton) || $(options.sendButton);
			var depDropDepend = container.find(options.depDropDepend) || $(options.depDropDepend);
			var nameInput	  = container.find(options.nameInput) || $(options.nameInput);

			//change input order for mobile version ( phone <--> city )
			if ($(window).width() < 768) {
				container.find($('.calculator__input-container.city'))
					.insertAfter(container.find($('.calculator__input-container.tel')));
			}

			//input type number polyfill
			numberInput.init();

			// prevent numbers and other unexpected symbols from name input
			onlyAlphaCharsInput(nameInput);
			
			//cashpoint city/department select code
			depDropDepend.depdrop({
				"url"		 :"http://cashpoint.ua/ru/page/department-list",
				"placeholder":"Отделение",
				"depends"    :[""+ options.depDropDepends +""],
				"loadingText":"Загрузка..."
			});

			//send button
			sendButton.click(function(){

				var nameContainer   = container.find($('.calculator__input-container.name'));
				var cityContainer   = container.find($('.calculator__input-container.city'));
				var telContainer    = container.find($('.calculator__input-container.tel'));
				var officeContainer = container.find($('.calculator__input-container.office'));
				var summContainer   = container.find($('.calculator__input-container.summ'));
				var timeContainer   = container.find($('.calculator__input-container.time'));
				var promoContainer	= container.find($('.calculator__input-container.promo'));

				var nameInput   = nameContainer.find('input');
				var cityInput   = cityContainer.find('select');
				var telInput    = telContainer.find('input');
				var officeInput = officeContainer.find('select');
				var summInput   = summContainer.find('input');
				var timeInput   = timeContainer.find('input');
				var promoInput	= promoContainer.find('input');

				var summIncorrectMessage = summContainer.find('.calculator__input-incorrect');
				var timeIncorrectMessage = timeContainer.find('.calculator__input-incorrect');

				var nameIsValid   = nameInput.val().length > 1;
				var telIsValid    = telInput.val().length === 19;
				var cityIsValid   = cityInput.val() != null;
				var officeIsValid = officeInput.val() != null;
				var summIsValid	  = (summInput.val() * 1) >= 200;
				var timeIsValid	  = (timeInput.val() * 1) >= 1;

				//remove red underline and warnings
				$('.calculator__input-container').removeClass('wrong');

				//apply if input is incorrect
				if (!nameIsValid) {
					nameContainer.addClass('wrong');
				}
				if (!telIsValid) {
					telContainer.addClass('wrong');
				}
				if (!cityIsValid) {
					cityContainer.addClass('wrong');
				}
				if (!officeIsValid) {
					officeContainer.addClass('wrong');
				}
				if (!summIsValid) {
					if (summInput.val() === '') {
						summIncorrectMessage.text('Необходимо указать сумму')
					}
					else if ((summInput.val()*1) < 200) {
						summIncorrectMessage.text('Мин. значение 200');
					}
					summContainer.addClass('wrong');
				}
				if (!timeIsValid) {
					if (timeInput.val() === '') {
						timeIncorrectMessage.text('Необходимо указать срок')
					}
					else if ((timeInput.val()*1) < 1) {
						timeIncorrectMessage.text('Мин. значение 1');
					}
					timeContainer.addClass('wrong');
				}

				//send if everything is correct
				if (nameIsValid && telIsValid && cityIsValid && officeIsValid && summIsValid && timeIsValid) {
					var data = {
						name: nameInput.val(),
						tel: telInput.val(),
						summ: summInput.val(),
						time: timeInput.val(),
						city: cityInput.val(),
						office: officeInput.val(),
						promo: promoInput.val() === '' ? 'Пользователь не ввел промокод' : promoInput.val()
					};
					$.ajax({
						url: './ajax/return_success.json',
						type: 'GET',
						data: JSON.stringify(data),
						success: function(res) {
							alert('Data: '+ JSON.stringify(data) + '\nresponse: ' + res.response)
						}
		            });
				}
			});	
		}
	}

});