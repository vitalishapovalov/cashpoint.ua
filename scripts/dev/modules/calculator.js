define(['jquery', 'numberInput', 'rangeslider', 'requestCalculator'],
	function($, numberInput, rangeslider, requestCalculator) {

    var sliders = {
        activeSum: {},
        activeTime: {}
    };

	return {

		initCalculator: function(container, options) {

            // credit calculator - input's type 'number' polyfill
            numberInput.init();

            var isMobile = $(window).width() < 768,
                body 	 = $('body');

		    // cache calculator's DOM nodes
            var output__credit_final_date  = container.find('span#credit_final_date'),
				output__credit_final_summ  = container.find('span#credit_final_summ'),
				output__credit_final_using = container.find('span#credit_final_using'),
                input__number_summ 		   = container.find('.number_summ_input'), //selects 2 inputs (for 'mobile devices' version)
				input__numder_date		   = container.find('#number_date'),
                button__credit_get 		   = container.find('#credit_get'),
                button__credit_type 	   = container.find('.credit_type > span'),
				slider__slider_summ  	   = container.find('.slider_summ > input[type="range"]'),
				slider__slider_date  	   = container.find('.slider_date > input[type="range"]'),
				block__input_slider		   = container.find('.input_sliders'),
                block__price_output 	   = container.find('.price_output'),
                credit_type_mobile  	   = container.find('.credit_type_mobile');

			var total, type, repayment;

			//first of all, set current expire date
            output__credit_final_date.text(function(){
			    var currentDate = new Date(),
			        finalDate   = new Date(currentDate.setDate(currentDate.getDate() + 10)),
			        days        = finalDate.getDate() < 10 ? '0' + finalDate.getDate() : finalDate.getDate(),
			        months 		= (finalDate.getMonth() + 1) < 10 ? '0' + (finalDate.getMonth() + 1) : (finalDate.getMonth() + 1);

                return days + '.' + months + '.' + finalDate.getFullYear()
			});

            // credit calculator - append additional number input on mobile devices
			if (isMobile) {
                input__number_summ
                    .clone()
                    .insertAfter(credit_type_mobile);
			}

			// credit calculator - cash/card switch
            button__credit_type.click(function(e) {

				var $this = $(this),
				    cashIndicator = 'cash',
				    cardIndicator = 'card',
				    text, mobileText;

                //ripple effect
                if (!isMobile) {
                    rippleEffect($this, e);
                }

				if (this.id === cashIndicator) {

                    block__price_output
						.removeClass(cardIndicator)
						.addClass(cashIndicator);

					text = 'ОТПРАВИТЬ ЗАЯВКУ';
					mobileText = 'наличными';
				}

				else if (this.id === cardIndicator) {

                    block__price_output
						.removeClass(cashIndicator)
						.addClass(cardIndicator);

					text = 'ПОЛУЧИТЬ ДЕНЬГИ';
					mobileText = 'на карту';
				}

				// re-count repayment sum
				countTotal();

                button__credit_get.text(text);

				if (isMobile) {
                    credit_type_mobile.text(mobileText);
				}
			});

			// Change slider value. executes when input value changes
			function changeSliderValueFromInput(item, input) {
				input.on('input', function(){
				    // valid input indicator
                    var valid_input = false;
                    // data
					var input_value = $(this).val(),
                        input_type = input.attr('data-input-type');
                    // check for min/max value
                    if ( input_type === 'sum' && +input_value >= 200 ) {
                        valid_input = true;
                    } else if ( input_type === 'time' && +input_value >= 1 ) {
                        valid_input = true;
                    }
					//execute only if not empty
					if ( input_value != '' && valid_input ) {
						//change value in slider
						item.update({
							from: input_value
						});
					}
				});
			}

			//count total payment. Exsecutes on every slide/input change
			function countTotal() {
				var summ = input__number_summ.val(),
				    days = input__numder_date.val(),
                    coef;

				if (block__price_output.hasClass('cash')) {

					type = 'cash';

			        if ( days <= 3 ) {
			            total = Math.floor( summ * 0.1 );
			        } 
			        else if( days <= 7 ) {
			            total  = Math.floor( summ * 0.14 + summ * 0.01 * days );
			        } 
			        else if( days <= 15 ) {
			            total  = Math.floor( summ * 0.30 + summ * 0.01 * days );
			        }	
				}

				else if (block__price_output.hasClass('card')) {

					type = 'card';

					if ( days <= 3 ) {
						coef = 0.0175;
					}
					else {
						coef = 0.015;
					}

					total = Math.round(days * summ * coef);
				}

				output__credit_final_using.text(total);

                if (typeof options === 'object') {
                    // user specified additional output fields
                    if (typeof options.output === 'object') {
                        // additional using outputs
                        if (typeof options.output.using === 'object') {
                            // additional using card (online) output
                            if (typeof options.output.using.card === 'object') {
                                if ( days <= 3 ) {
                                    coef = 0.0175;
                                } else {
                                    coef = 0.015;
                                }
                                options.output.using.card.text(Math.round(days * summ * coef));
                            }
                        }
                        // additional credit sum output
                        if (typeof options.output.sum === 'object') {
                            options.output.sum.text(summ);
                        }
                        // additional credit time output
                        if (typeof options.output.time === 'object') {
                            options.output.time.text(days);
                        }
                    }

                }
			}

			//change number inputs and outputs values. executes on slider change
			function changeValues(data, min, input, output) {
			    if (data > min) {
			    	//change input val
				    input.val(data);
			    }
			    //change output val
			    //if its date
			    if ( min === 1 ) {
			    	var currentDate = new Date();
			    	var finalDate   = new Date(currentDate.setDate(currentDate.getDate() + data));
			    	var days        = finalDate.getDate() < 10 ? '0' + finalDate.getDate() : finalDate.getDate();
			    	var months 		= (finalDate.getMonth() + 1) < 10 ? '0' + (finalDate.getMonth() + 1) : (finalDate.getMonth() + 1);

			    	data = (days + '.' + months + '.' + finalDate.getFullYear());
			    	repayment = data;
			    }

				output.text(data);
				countTotal(); 
			}

			// credit summ slider
			slider__slider_summ.ionRangeSlider({
			    min: 200,
			    max: 7000,
				step: 100,
			    onChange: function(data) {
					changeValues(data.from, 200, input__number_summ, output__credit_final_summ);
			    },
			    onFinish: function(data) {
					input__number_summ.val(data.from);
					countTotal();
			    },
			    onUpdate: function(data) {
					changeValues(data.from, 200, input__number_summ, output__credit_final_summ);
			    }
			});
			// Saving it's instance to var
			sliders.activeSum = slider__slider_summ.data("ionRangeSlider");
			//set it on page load
            sliders.activeSum.update({
				from: 1000
			});

			// credit time slider
			slider__slider_date.ionRangeSlider({
			    min: 1,
			    max: 15,
			    onStart: function() {
					block__input_slider.removeClass('loading');
			    },
			    onChange: function(data) {
					changeValues(data.from, 1, input__numder_date, output__credit_final_date);
			    },
			    onFinish: function(data) {
					input__numder_date.val(data.from);
					countTotal();
			    },
			    onUpdate: function(data) {
					changeValues(data.from, 1, input__numder_date, output__credit_final_date);
			    }
			});
			// Saving it's instance to var
			sliders.activeTime = slider__slider_date.data("ionRangeSlider");

			// Sum input
			changeSliderValueFromInput(sliders.activeSum, input__number_summ);
			// Date input
			changeSliderValueFromInput(sliders.activeTime, input__numder_date);

			// write selected data to cookie
			button__credit_get.click(function(){
				var data = {
					type: type,
					pay: total,
					repayment: repayment,
					credit: input__number_summ.val(),
					time: input__numder_date.val()
				};

				for ( var item in data )
					localStorage.setItem(item, data[item])

				// open register/login modal if 'card' type was chosen
				if (block__price_output.hasClass('card')) {
					if (!body.hasClass('logged')) {
						openModal('modal_register-login');
					}
				}
				// open cash credit request modal if 'credit' type was chosen
				else if(block__price_output.hasClass('cash')) {
					requestCalculator.pushValues({
						container: $('.modal__credit-cash'),
						summ: input__number_summ.val(),
						time: input__numder_date.val()
					});
					openModal('modal_credit-cash');
				}
			});
		},

        set: {
		    values: function(sum, time){
                sliders.activeSum.update({
                    from: sum
                });
                sliders.activeTime.update({
                    from: time
                });
            }
        }
	}
});