define(['jquery'], function($) {

	return {

		init: function() {

			window.initMap = function() {

				//styling map
				var style = [
					{
					    "featureType": "administrative.country",
					    "elementType": "geometry",
					    "stylers": [
					        {
					            "visibility": "simplified"
					        },
					        {
					            "hue": "#ff0000"
					        }
					    ]
					}
				];

				//create map
				map = new google.maps.Map(document.getElementById('map'), {
						center: {lat: 50.449872, lng: 30.523718},
						scrollwheel: false,
						zoom: 15,
						styles: style
				});

				//create markers
				var icon = 'img/map-icon.png';
				var markers = [{
					position: {lat: 50.449872, lng: 30.523718},
					title: 'Отделение №: 322',
					address: 'пл. Дружбы Народов, 4',
					workTime: '09:00 - 21:00',
					findUs: 'Модал 1'
			  	},
			  	{
			  		position: {lat: 50.449872, lng: 30.529718},
					title: 'Отделение №: 148',
					address: 'пл. Дружбы Народов, 6',
					workTime: '66:66 - 66:66',
					findUs: 'Модал 2'
			  	}];
				var i = 0;
				var mapMarkers = [];
			  	for (i; i < markers.length; i++) {
					//set icon
					markers[i].icon = icon;
			  		//bind to the main map object
					markers[i].map = map;
					//create markers
					mapMarkers[i] = new google.maps.Marker(markers[i]);
					//open modal window on click (each marker has its own)
		        	google.maps.event.addListener(mapMarkers[i], 'click', function() {
		        		openOfficeModal(this.title, this.address, this.workTime, this.findUs);
					});
			    }

			};
			
			//append google maps api v3 script
			$('body').append('<script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrFjHOCFQbsMvKEGL92MTAffwFyHgogZI&language=ru&callback=initMap"></script>');	
		},
	}

});