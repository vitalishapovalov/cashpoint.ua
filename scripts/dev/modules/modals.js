define(['jquery', 'maskedInput', 'calculator'], function($, maskedInput, calculator){

	function isDevice(){
		return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
	}

	function hideAddressBar(){
		if(document.documentElement.scrollHeight<window.outerHeight/window.devicePixelRatio) {
			if ( isDevice() ) {
				document.documentElement.style.height=(window.outerHeight/window.devicePixelRatio)+'px';
				setTimeout(window.scrollTo(1,1),0);
			}
		}

	}

	return {

		initModals: function() {

			// hide addressbar on scroll
			$('.modal__container_all').on('scroll', function(){
				hideAddressBar();
			});

			//phone number mask
			$('.modal__container_all input[type="tel"]').mask("+38 (099) 999-99-99");
			
			var body = $('body'),
				modal__container_all = $('.modal__container_all');

			//check if the page has 'Calculator' modal and init scripts
			if ($('.modal-window.modal__calculator').length) {
				// init calculator(with range sliders) scripts
				calculator.initCalculator(modal__container_all.find('.price_box'));
			}

			// global variables for other modules
			window.closeModals = function() {
				body.removeClass('modal modal_agreement modal_login modal_password modal_register modal_register-login modal_office modal_calculator modal_credit-cash modal_question modal_request-rework modal_cannot_modify_data');
				$('.modal__input-container').removeClass('incorrect');
			};
			window.openModal = function(modal) {
				closeModals();
				body.addClass('modal ' + modal);
				hideAddressBar();
			};
			window.openOfficeModal = function(office, address, workTime, findUs) {
				var modal = $('.modal__office');

				modal.find('.modal__title').text(office);
				modal.find('#modal__address-output').text(address);
				modal.find('#modal__workTime-output').text(workTime);
				modal.find('#modal__findUs-output').text(findUs);

				openModal('modal_office');
			};

			// wrong input warning
			function warn(container, message) {
				if (typeof message === 'string') {
					container.find('.modal__label-warning').text(message);
				}
				container.addClass('incorrect');
			}

			// clear all inputs in modal
			function clearinputs( modal ) {
				if ( modal.find('input') )
					modal.find('input').val('');
				if ( modal.find('select') )
					modal
						.find('select')
						.val(0);
				if ( modal.find('textarea') )
					modal.find('textarea').val('');
			}

			// input validation functions
			window.loginValidation = function(element) {
				var container = element.parent();

				var phoneInputContainer    = container.find('.modal__input-container.phone');
				var passwordInputContainer = container.find('.modal__input-container.password');

				var phoneInput    = phoneInputContainer.find('input');
				var passwordInput = passwordInputContainer.find('input');

				container.find('.modal__input-container').removeClass('incorrect');

				if (phoneInput.val().length === 0) {
					warn(phoneInputContainer);
				}
				if (passwordInput.val().length < 6) {
					warn(passwordInputContainer, 'Минимальная длинна пароля - 6 символов');
				}
				if (phoneInput.val().length === 19 && passwordInput.val().length >= 6) {
					//generate data object
					var data = JSON.stringify({
						phone: phoneInput.val(),
						password: passwordInput.val()
					});
					//send data
					$.ajax({
						url: './ajax/return_success.json',
						type: 'POST',
						data: data,
						success: function(res) {
							alert('Data: '+ data + '\nresponse: ' + res.response)
						}
					})
				}	
			};

			function registrationValidation(element) {
				var container = element.parent();

				var phoneInputContainer    = container.find('.modal__input-container.phone'),
					emailInputContainer	   = container.find('.modal__input-container.email'),
					passwordInputContainer = container.find('.modal__input-container.password'),
					confirmInputContainer  = container.find('.modal__input-container.password_confirm');

				var phoneInput    = phoneInputContainer.find('input'),
					emailInput 	  = emailInputContainer.find('input'),
					passwordInput = passwordInputContainer.find('input'),
					confirmInput  = confirmInputContainer.find('input');

				var correctPhone, correctEmail, correctPassword, correctConfirm, uniquePhone, uniqueEmail;

				container.find('.modal__input-container').removeClass('incorrect');

				// phone number validation + unique validation
				if (phoneInput.val().length === 0) {
					warn(phoneInputContainer);
				}
				else {
					correctPhone = true;
				}

				// email validation
				if (!emailInput.val().length > 0) {
					warn(emailInputContainer, 'Необходимо указать email');
				}
				else if (!isValidEmail(emailInput.val())) {
					warn(emailInputContainer, 'Введите корректный email');	
				}
				else if (isValidEmail(emailInput.val())) {
					correctEmail = true;
				}

				// password validation
				if (passwordInput.val().length === 0) {
					warn(passwordInputContainer, 'Необходимо указать пароль');	
				}
				else if (passwordInput.val().length < 6) {	
					warn(passwordInputContainer, 'Минимальная длинна пароля - 6 символов');	
				}
				else if (passwordInput.val() != confirmInput.val()) {
					warn(confirmInputContainer);
				}
				else if (correctPhone && correctEmail) {
					//generate data
					var data = {
						phone: phoneInput.val(),
						email: emailInput.val(),
						password: passwordInput.val()
					};
					//check phone number (uniqueness)
					$.ajax({
						url: './ajax/return_success.json',
						type: 'GET',
						data: JSON.stringify({
							phone: data.phone,
							email: data.email,
							event: 'unique'
						}),
						success: function(res) {
							console.log('Data: '+ JSON.stringify({phone:data.phone,email:data.email,event:'unique'}) + '\nresponse: ' + res.response);
							if (res.response === "success") {
								uniquePhone = true;
							}
							else {
								warn(phoneInputContainer, "Этот номер телефона уже есть в базе");
							}
							if (res.response === "success") {
								uniqueEmail = true;
							}
							else {
								warn(emailInputContainer, 'Этот email уже есть в базе');	
							}
						}
					}).then(function(){
						if (uniquePhone && uniqueEmail) {
							// send data
							$.ajax({
								url: './ajax/return_success.json',
								type: 'GET',
								data: JSON.stringify({
									new_user_data: data,
									event: 'new_user'
								}),
								success: function(res) {
									alert('Registration successful. \nData: '+ JSON.stringify({new_user_data:data,event:'new_user'}) + '\nresponse: ' + res.response)
								}
							});	
						}
					});
				}	
			}

			function checkPasswordOnInput(element) {
				var container = element.parent().parent();

				var passwordContainer = container.find('.modal__input-container.password');
				var confirmContainer  = container.find('.modal__input-container.password_confirm');

				var passwordInput = passwordContainer.find('input');
				var confirmInput = confirmContainer.find('input');

				if (passwordInput.val().length < 6) {
					warn(passwordContainer, 'Минимальная длинна пароля - 6 символов');
					return false;
				}
				else if (passwordInput.val() != confirmInput.val()) {
					passwordContainer.removeClass('incorrect');
					warn(confirmContainer, 'Пароли не совпадают');	
					return false;
				}
				else {
					passwordContainer.removeClass('incorrect');
					confirmContainer.removeClass('incorrect');
					return true;
				}	
			}

			// 'X' button on modals to close them
			body.on('click tap', '.modal__close', function(){
				closeModals();
			});

			// modal login window
			body.on('click tap', '.modal__auth', function(){
				openModal('modal_login');
			});

			// modal password recovery window
			body.on('click tap', '.modal__passwordRecovery', function(){
				openModal('modal_password');
			});

			// modal registration window
			body.on('click tap', '.modal__registration', function(){
				openModal('modal_register');
			});

			// modal ask a question window
			body.on('click tap', '.modal__askQuestion', function(){
				openModal('modal_question');
			});
			
			// modal credit calculator window
			body.on('click tap', '.modal__requestRework', function(){
				openModal('modal_request-rework');
			});

			// modal with error (request-rework)
			body.on('click tap', '.modal__openCalculator', function(){
				openModal('modal_calculator');
			});

			// modal with agreement
			body.on('click tap', '.modal__showAgreement', function(){
				openModal('modal_agreement');
			});

			/* SUBMIT BUTTONS INSIDE MODALS */

			// password recovery 
			$('#modal__password').click(function(){
				var inputContainer = $('.modal__password').find('.modal__input-container');
				var input 		   = inputContainer.find('input');

				var value = input.val();

				inputContainer.removeClass('incorrect');

				if (value.length > 0) {
					if (isValidEmail(value)) {
						$.ajax({
							url: './ajax/return_success.json',
							type: 'GET',
							data: JSON.stringify({
								password: value,
								event: "password_recover"
							}),
							success: function(res) {
								alert('Data: '+ JSON.stringify({password:value,event:"password_recover"}) + '\nresponse: ' + res.response)
							}
						})
					}
					else {
						warn(inputContainer, 'Введите корректный email');
					}
				}
				else {
					warn(inputContainer, 'Необходимо указать email');
				}
			});

			// ask a question window validation
			// prevent numbers and other unexpected symbols to be entered
			onlyAlphaCharsInput($('.modal__question .modal__input-container.name input'));
			// validate fields on click
			$('#modal__askQuestion').click(function(){

				var container = $(this).parent().parent();
				var honorificContainer = container.find('.modal__input-container.honorific');
				var nameContainer 	   = container.find('.modal__input-container.name');
				var emailContainer 	   = container.find('.modal__input-container.email');
				var phoneContainer     = container.find('.modal__input-container.phone');
				var messageContainer   = container.find('.modal__input-container.message');

				var honorificIsValid = +honorificContainer.find('select').val() != 0;
				var nameIsValid		 = nameContainer.find('input').val().length > 1;
				var messageIsValid	 = messageContainer.find('textarea').val().length > 1;
				var phoneIsValid	 = phoneContainer.find('input').val().length === 19;
				var emailIsFilled	 = emailContainer.find('input').val().length != 0;
				var emailIsValid	 = isValidEmail(emailContainer.find('input').val()),
					emailIsValidated = false;

				container.find('.modal__input-container').removeClass('incorrect');
				
				if (!honorificIsValid) {
					warn(honorificContainer);
				}

				if (!nameIsValid) {
					warn(nameContainer);
				}

				if (!phoneIsValid) {
					warn(phoneContainer);
				}

				if (!messageIsValid) {
					warn(messageContainer);
				}

				if (!emailIsFilled) {
					warn(emailContainer, 'Необходимо указать email');
				} else if (emailIsFilled && !emailIsValid) {
					warn(emailContainer, 'Введите корректный email');
				} else {
					emailIsValidated = true;
				}

				var allFieldsAreValid = honorificIsValid && nameIsValid && messageIsValid && phoneIsValid && emailIsValidated;

				if (allFieldsAreValid) {
					var data = {
						honorific: honorificContainer.find('select').val(),
						name: nameContainer.find('input').val(),
						email: emailContainer.find('input').val(),
						phone: phoneContainer.find('input').val(),
						message: messageContainer.find('textarea').val()
					};
					$.ajax({
						url: './ajax/return_success.json',
						type: 'GET',
						data: JSON.stringify(data),
						success: function(res) {
							console.log('Data: '+ JSON.stringify(data) + '\nresponse: ' + res.response);
							closeModals();
							clearinputs( container );
						}
					})
				}

			});

			// login window validation
			$('#modal__login').click(function(){
				loginValidation($(this));
			});

			// register window validation
			$('#modal__register').click(function(){
				registrationValidation($(this));
			});

			// login block on register-login form validation
			$('#modal__register-login_login').click(function(){
				loginValidation($(this));
			});

			// register block on register-login form validation
			$('#modal__register-login_register').click(function(){
				registrationValidation($(this));
			});

			// password and confirm password inputs on register forms validation-on-typing
			$('.modal__register .modal__input-container.password input, .modal__register .modal__input-container.password_confirm input, .modal__single-modal.register .modal__input-container.password input, .modal__single-modal.register .modal__input-container.password_confirm input').on('input', function(){
				checkPasswordOnInput($(this));
			});	

	 		// hide calculator modal if clicked on black overlay
	 		modal__container_all.click(function(e){
	 			if (!($(e.target).closest('.modal__calculator').length > 0) && body.hasClass('modal_calculator')) {
					closeModals();
	 			}
	 		});

	 		// hide modal with error (request-rework) if clicked on black overlay
	 		modal__container_all.click(function(e){
	 			if (!($(e.target).closest('.modal__request-rework').length > 0) && body.hasClass('modal_request-rework')) {
					closeModals();
	 			}
	 		});

			// hide agreement if clicked on black overlay
			modal__container_all.click(function(e){
				if (!($(e.target).closest('.modal__agreement').length > 0) && body.hasClass('modal_agreement')) {
					closeModals();
				}
			});

			// hide office modal (google maps) if clicked on black overlay
			modal__container_all.click(function(e){
				if (!($(e.target).closest('.modal__office').length > 0) && body.hasClass('modal_office')) {
					closeModals();
				}
			});
		}
	}
});

