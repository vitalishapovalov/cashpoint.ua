require(['jquery', 'viewportUnitsBuggyfill', 'hammerTime', 'modals', 'requestCalculator',
	'mainPage', 'aboutPage', 'addressPage', 'creditsPage', 'personalCabinetPages', 'paymentPage', 'faqPage'],

	function($, viewportUnitsBuggyfill, hammerTime, modals, requestCalculator,
		mainPage, aboutPage, addressPage, creditsPage, personalCabinetPages, paymentPage, faqPage) {

	//viewport units fix
	viewportUnitsBuggyfill.init();

	// cache for fast access
	var body = $('body'),
		main = $('main');


 	//main page scripts
	var isMainPage = main.attr('id') === 'cp_main';

	//credits page scripts
	var isCreditsPage = main.attr('id') === 'cp_credits';

	//about page scripts
	var isAboutPage = main.attr('id') === 'cp_about';

	//address page scripts
	var isAddressPage = main.attr('id') === 'cp_address';

	// payment page scripts
	var isPaymentPage = main.attr('id') === 'cp_payment';

	// FAQ pages scripts
	var isFaqPage = main.attr('id') === 'cp_faq';

	//personal cabinet pages scripts
	var isPersonalCabinetPage = main.attr('id') === 'cp_personal' && body.hasClass('logged');


/*=======================================================================
========================== MAIN PAGE SCRIPTS ============================
=======================================================================*/

 	if (isMainPage) {
 		mainPage.init();
 	}

/*=======================================================================
========================== CREDITS PAGE SCRIPTS =========================
=======================================================================*/

	if (isCreditsPage) {
		creditsPage.init();
	}

/*=======================================================================
========================== ABOUT PAGE SCRIPTS ===========================
=======================================================================*/

	if (isAboutPage) {
		aboutPage.init();
	}

/*=======================================================================
========================== ADDRESS PAGES SCRIPTS =========================
=======================================================================*/

	if (isAddressPage) {
		addressPage.init();
	}

/*=======================================================================
========================== PAYMENT PAGE SCRIPTS =========================
=======================================================================*/

    if (isPaymentPage) {
        paymentPage.init();
    }

/*=======================================================================
========================== PAYMENT PAGE SCRIPTS =========================
=======================================================================*/

	if (isFaqPage) {
		faqPage.init();
	}

/*=======================================================================
===================== PERSONAL CABINET PAGES SCRIPTS ====================
=======================================================================*/

	if (isPersonalCabinetPage) {
		// current credits page
		if ( main.hasClass('current_credits') )
			personalCabinetPages.currentCreditsPageInit();
		// credit card input/check page
		else if ( main.hasClass('credit_card_check') )
			personalCabinetPages.creditCardCheckInit();
		// Your request has been accepted page
		else if ( main.hasClass('request_accepted') )
			personalCabinetPages.requestAcceptedInit();
		// Get credit steps pages
		else if ( main.hasClass('getCredit_steps') )
			personalCabinetPages.getCreditStepsInit();
		// cabinet settings page
		else if ( main.hasClass('cabinet_settings') )
			personalCabinetPages.cabinetSettingsInit();
	}

/*=======================================================================
========================== COMMON SCRIPTS ===============================
=======================================================================*/

	$('.numeric').on('keypress keyup', function(){
		var value = this.value,
			output;
		output = value.replace(/\D/g,'');
		this.value = output;
	});

	body.on('change', 'select', function(){
		$(this).addClass('active');
	});

	//ripple effects
	$('.ripple-button').click(function(e){
		var t = $(this);
		rippleEffect(t, e)
	});

	var personal_cabinet_button = $('#personal_cabinet'),
		personal_cabinet_sideMenu = $('.personal_cabinet_menu');
	//personal cabinet interactions
	personal_cabinet_button.hover(
		function() {
			personal_cabinet_sideMenu.addClass('opened')
		},
		function() {
			personal_cabinet_sideMenu.removeClass('opened')
		}
	);
	$(document).on('click tap', function(e){
		var notMenu = !($(e.target).closest(personal_cabinet_button).length > 0);
		var menuOpened  = personal_cabinet_sideMenu.hasClass('opened');
		if (notMenu && menuOpened)
			personal_cabinet_sideMenu.removeClass('opened')
	});

	//side slide menu (mobile-only)
	(function(){
		var showButton = $('.header_menu_icon');
		var hideButton = $('.side_menu_close');
		var menuBody   = $('.side_menu');

		var indicatorItem  = $('body');
		var indicatorClass = 'side_menu_active';


		var dropdown = $('.side_personal_cabinet'); //dropdown menu (Личный кабинет)

		function showMenu() {
			indicatorItem.addClass(indicatorClass)
		}

		function hideMenu() {
			indicatorItem.removeClass(indicatorClass)
		}

		showButton.click(function(){
			menuBody.css('visibility', 'visible');
			showMenu()
		});

		hideButton.click(function(){
			hideMenu()
		});

		$('.black_bg').click(function(){
			hideMenu()
		});

		$(document).click(function(e){
			var notMenu = !(($(e.target).closest(menuBody).length > 0) || ($(e.target).closest(showButton).length > 0));
			var menuOpened  = menuBody.hasClass(indicatorClass);
			if (notMenu && menuOpened)
				hideMenu()
		});

		dropdown.click(function(){
			var indicatorClass = 'opened';
				if (!$(this).hasClass(indicatorClass))
					$(this).addClass(indicatorClass);
				else
					$(this).removeClass(indicatorClass)

		});

	})();

/*=======================================================================
====================== MODAL WINDOWS SCRIPTS ============================
=======================================================================*/

	// init modal windows scripts
	modals.initModals();

	// init cash calculator validation script on cash-credit modal.
	// credits page already has this script
	var options = {
		container: 		$('.modal__credit-cash'),
		sendButton: 	'#calculator__send',
		depDropDepend:  '#guestcredit-department_id',
		depDropDepends: 'guestcredit-city_id-modal',
		nameInput: 	    '.calculator__input-container.name input',
		summInputId: 	'calculator__summ-input-modal',
		timeInputId: 	'calculator__time-input-modal'
	};
	requestCalculator.initRequestCalculator(options);

});

// GLOBAL FUNCTIONS ---------------------------------------------------------------------------------------------------------

	//email verification
	window.isValidEmail = function(emailAddress) {
	    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	    return pattern.test(emailAddress);
	};

 	// Ripple effect on click. element to append must have 'position: relative'
	window.rippleEffect = function(element, e) {

		var ink, d, x, y;

		//create .ink element if it doesn't exist
		if(element.find(".ink").length == 0) {
			element.prepend("<span class='ink'></span>");
		}
		ink = element.find(".ink");

		//incase of quick double clicks stop the previous animation
		ink.removeClass("animate");

		//set size of .ink
		if(!ink.height() && !ink.width())
		{
			//use parent's width or height whichever is larger
			d = Math.max(element.outerWidth(), element.outerHeight());
			ink.css({height: d, width: d});
		}

		//get click coordinates
		x = e.pageX - element.offset().left - ink.width()/2;
		y = e.pageY - element.offset().top - ink.height()/2;
		//set the position and add class .animate
		ink.css({top: y+'px', left: x+'px'}).addClass("animate");
	};

	// prevent numbers and other unexpected symbols to be entered
	window.onlyAlphaCharsInput = function(input) {
		input.on("keydown", function(event){
				var arr = [8,16,17,20,35,36,37,38,39,40,45,46,32,30,189,229, 0];
				for(var i = 65; i <= 90; i++){
				  arr.push(i);
				}
				if(jQuery.inArray(event.which, arr) === -1){
				  event.preventDefault();
				}
		});
		input.on('keypress keyup', function(){
			var value = this.value,
				output;
			output = value.replace(/[0-9]/g, "");
			this.value = output;
		})
	};