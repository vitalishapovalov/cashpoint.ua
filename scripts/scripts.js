requirejs.config({
    baseUrl: 'scipts',
    waitSeconds: 25,
    paths: {
        ////modules
        //jQuery and its deps
        jquery: 'dep/jquery-3.1.0.min',
        rangeslider: 'dep/rangeslider',
        bxSlider: 'dep/jquery.bxslider',
        //viewport units fix
        viewportUnitsBuggyfill: 'dep/viewport-units-buggyfill',
        //polyfill for input type=number
        numberInputPolyfill: 'dep/numberInputPolyfill',
        //hammertime
        hammerTime: 'dep/hammertime.min',
        //phone numbers mask
        maskedInput: 'dep/maskedinput',
        //dependent dropdown
        depdrop: 'dep/dependentdropdown',
        // credit card validation
        creditCardValidation: '../js/creditcardjs',
        // dropzone plugin
        dropzone: '../js/dropzone',
        // webcam plugin
        webcam: '../js/webcam',

        ////custom modules
        numberInput: 'dev/modules/numberInput',
        readyBxSlider: 'dev/modules/ready_bxSlider',
        googleMap: 'dev/modules/googleMap',
        calculator: 'dev/modules/calculator',
        modals: 'dev/modules/modals',
        requestCalculator: 'dev/modules/requestCalculator',

        ////pages
        mainPage: 'dev/pages/main',
        aboutPage: 'dev/pages/about',
        addressPage: 'dev/pages/address',
        creditsPage: 'dev/pages/credits',
        paymentPage: 'dev/pages/payment',
        faqPage: 'dev/pages/faq',

        // dont bundle scripts below
        personalCabinetPages: 'dev/pages/personal',
        getCreditSteps: '../js/pc_credit_steps',
        datepicker: '../js/datepicker',

        ////entry point
        index: 'dev/index'
    }
});

requirejs(['index']);