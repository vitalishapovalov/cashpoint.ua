'use strict';

var gulp     	   = require('gulp');
var runSequence    = require('run-sequence');
var sourcemaps     = require('gulp-sourcemaps');
var plumber        = require('gulp-plumber');
//css
var concat 	 	   = require('gulp-concat');
var cleanCSS 	   = require('gulp-clean-css');
var autoprefixer   = require('gulp-autoprefixer');
var sass 	 	   = require('gulp-sass');
var pleeease       = require('gulp-pleeease');
//js
var exec           = require('child_process').exec;
var babel          = require('gulp-babel');
var uglify         = require('gulp-uglify');
//html
var pug            = require('gulp-pug');
// live-ftp-upload
var connect        = require('gulp-connect');
var ftp            = require('vinyl-ftp');
var gutil          = require('gulp-util');
//----------------------------------------FTP TASKS----------------------------------------//

// vars for server connection and deploy task
var user = process.env.FTP_USER;  
var password = process.env.FTP_PWD;  
var host = 'ftp.chis.kiev.ua';
var port = 21;
var localFilesGlob = ['www/**/*'];  
var remoteFolder = 'public_html/cashpoint';

// build an FTP connection
function getFtpConnection() {  
    return ftp.create({
        host: host,
        port: port,
        user: user,
        password: password,
        parallel: 5,
        log: gutil.log
    });
}

// upload files to the server on every file change in www/
gulp.task('ftp-deploy-watch', function() {
    var conn = getFtpConnection();
    gulp.watch(localFilesGlob)
    .on('change', function(event) {
      console.log('Changes detected! Uploading file "' + event.path + '", ' + event.type);
      return gulp.src( [event.path], { base: '.', buffer: false } )
        .pipe(plumber())
        .pipe( conn.newer( remoteFolder ) ) // only upload newer files 
        .pipe( conn.dest( remoteFolder ) )
      ;
    });
});

//----------------------------------------CSS TASKS----------------------------------------//

// Main SASS files
var sassFilesPath = [
				  'styles/scss/all.scss'
			   ];

// Concat files + minify + autoprefix css files			   
function css_production(path, newName) {
	return gulp.src(path, {base: './styles/css'})
         .pipe(plumber())
         .pipe(sourcemaps.init())
			   .pipe(concat(newName))
			   .pipe(cleanCSS())
			   .pipe(autoprefixer({
          browsers: ['last 5 version', 'safari 5', 'ie 8', 'ie 9', 'Android 4.2', 'Android 2.3']
         }))
         .pipe(pleeease({
            "in": "all.min.css",
            "out": "styles.min.css",
            "browsers": ["last 3 versions", "Android 2.3", "ie 8", "Firefox ESR", "Opera 12.1", "Chrome > 20", "ios 4"]
         }))
         .pipe(sourcemaps.write('.'))
			   .pipe(gulp.dest('./www/css/'));
}

// Compile SASS
gulp.task('sass_compile', function() {
	return	gulp.src(sassFilesPath)
          .pipe(plumber())
			  	.pipe(sass({
			  		errLogToConsole: true,
			  		includePaths: ['styles/scss']
			  	}))
			  	.pipe(gulp.dest('styles/css'));
});

// Concat + minify + autoprefix css
// styles.css
gulp.task('css_compile', function() {
	return css_production('styles/css/all.css', 'all.min.css');
});

// CSS production task
gulp.task('css', function() {
  runSequence('sass_compile',
              'css_compile');
});

// Watch .scss files
gulp.task('watch-css', function() {
  gulp.watch(['styles/scss/**/*.scss'], ['css']);
});

//-----------------------------------------JS TASKS ----------------------------------------//

// Concat + minify js
gulp.task('rjs', function (cb) {
  exec('node tools/r.js -o tools/build.js', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

//minify modules
gulp.task('modules-minify', ()=> {
    return gulp.src('scripts/dev/pages/not-concat/**.js')
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('www/js'));
});

// Watch .js files
gulp.task('watch-js', function() {
  gulp.watch('scripts/**/*.js', ['rjs', 'modules-minify']);
});

//watch modules
gulp.task('watch-jsModules', function() {
    gulp.watch('scripts/dev/pages/not-concat/*.js', ['modules-minify']);
});

//-----------------------------------------HTML TASKS---------------------------------------//

//compile .pug to .html
gulp.task('pug_compile', function buildHTML() {
  return gulp.src(['templates/**/*.pug',
                   '!templates/_includes/**/*.pug'])
        .pipe(plumber())
        .pipe(pug({
          pretty: true,
          nspaces: 4,
          tabs: true,
          donotencode: true
        }))
        .pipe(gulp.dest('./www/'));
});

// watch .pug files
gulp.task('watch-html', function() {
  gulp.watch('templates/**/*.pug', ['pug_compile']);
});

//-----------------------------------------DEFAULT TASK------------------------------------//

// developer mode 
gulp.task('dev', function() {
  runSequence('watch-css',
              'watch-js',
              'watch-html');
});

// developer mode with ftp deploy
gulp.task('dev-ftp', function() {
  runSequence('watch-css',
              'watch-js',
              'watch-html',
              'ftp-deploy-watch');
});